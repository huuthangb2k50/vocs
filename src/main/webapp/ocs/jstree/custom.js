var TreeType = {
		RATE_TABLE: 20,
		NORMALIZERS: 22
};

(function($) {
	$.fn.ocsTree = function(treeType) {
		this.jstree({
			'core' : {
				'data' : {
					'url' : 'nestedsets/getNode',
					'data' : function (node) {
						return { 'treeType' : treeType, 'id' : node.id == '#' ? 0 : node.id };
					}
				}
			},
		});
	};
})(jQuery);