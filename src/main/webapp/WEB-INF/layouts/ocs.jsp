
<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib tagdir="/WEB-INF/tags/util" prefix="util" %>

<html>  		
	<head>
		<title>VOCS - Admin</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />	
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<util:load-scripts />
		<!-- Bootstrap 3.3.6 -->
		<spring:url value="/resources/ocs/bootstrap/css/bootstrap.min.css" var="bootstrap_min_css"></spring:url>
		<link rel="stylesheet" href="${bootstrap_min_css}">
		<!-- Font Awesome -->
		<spring:url value="/resources/ocs/fonts/css/font-awesome.css" var="font_awesome_css"></spring:url>	
		<link rel="stylesheet" href="${font_awesome_css}">
		<!-- Ionicons -->
		<spring:url value="/resources/ocs/fonts/css/ionicons.css" var="ionicons_css"></spring:url>
		<link rel="stylesheet" href="${ionicons_css}">
		<!-- Font themify -->
		<spring:url value="/resources/ocs/fonts/css/themify-icons.css" var="themify_icons_css"></spring:url>
		<link rel="stylesheet" href="${themify_icons_css}">
		<!-- Theme style -->
		<spring:url value="/resources/ocs/dist/css/AdminLTE.min.css" var="AdminLTE_css"></spring:url>
		<link rel="stylesheet" href="${AdminLTE_css}">
		<!-- AdminLTE Skins. Choose a skin from the css/skins
		     folder instead of downloading all of them to reduce the load. -->
		<spring:url value="/resources/ocs/dist/css/skins/_all-skins.min.css" var="_all_skins_css"></spring:url>
		<link rel="stylesheet" href="${_all_skins_css}">
		<!-- iCheck -->
		<spring:url value="/resources/ocs/plugins/iCheck/flat/blue.css" var="blue_css"></spring:url>
		<link rel="stylesheet" href="${blue_css}">
		<!-- Morris chart -->
		<spring:url value="/resources/ocs/plugins/morris/morris.css" var="morris_css"></spring:url>
		<link rel="stylesheet" href="${morris_css}">
		<!-- Date Picker -->
		<spring:url value="/resources/ocs/plugins/datepicker/datepicker3.css" var="datepicker3_css"></spring:url>
		<link rel="stylesheet" href="${datepicker3_css}">
		<!-- Daterange picker -->
		<spring:url value="/resources/ocs/plugins/daterangepicker/daterangepicker.css" var="daterangepicker_css"></spring:url>
		<link rel="stylesheet" href="${daterangepicker_css}">
		<!-- bootstrap wysihtml5 - text editor -->
		<spring:url value="/resources/ocs/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" var="bootstrap3_wysihtml5_css"></spring:url>
		<link rel="stylesheet" href="${bootstrap3_wysihtml5_css}">
		<!-- jstree -->
		<spring:url value="/resources/ocs/jstree/themes/default/style.min.css" var="jstree_css"></spring:url>
		<link rel="stylesheet" href="${jstree_css}">		
		<!-- Custorm Csss -->
		<spring:url value="/resources/ocs/dist/css/custom.css" var="custom_css"></spring:url>
		<link rel="stylesheet" href="${custom_css}">
		<!-- jQuery 2.2.3 -->
		<spring:url value="/resources/ocs/plugins/jQuery/jquery-2.2.3.min.js" var="jquery_2_2_3_js"></spring:url>
		<script src="${jquery_2_2_3_js }"></script>
		<!-- jQuery UI 1.11.4 -->
		<spring:url value="/resources/ocs/dist/js/jquery-ui.min.js" var="jquery_ui_js"></spring:url>
		<script src="${jquery_ui_js }"></script>
		<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
		<script>
			$.widget.bridge('uibutton', $.ui.button);
		</script>
		<!-- Bootstrap 3.3.6 -->
		<spring:url value="/resources/ocs/bootstrap/js/bootstrap.min.js" var="bootstrap_js"></spring:url>
		<script src="${ bootstrap_js}"></script>
		<!-- Morris.js charts -->
		<spring:url value="/resources/ocs/dist/js/raphael-min.js" var="raphael_min_js"></spring:url>
		<script src="${ raphael_min_js}"></script>
		<spring:url value="/resources/ocs/plugins/morris/morris.min.js" var="morris_js"></spring:url>
		<script src="${morris_js }"></script>
		<!-- Sparkline -->
		<spring:url value="/resources/ocs/plugins/sparkline/jquery.sparkline.min.js" var="jquery_sparkline_js"></spring:url>
		<script src="${jquery_sparkline_js }"></script>
		<!-- jvectormap -->
		<spring:url value="/resources/ocs/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" var="jquery_jvectormap_1_2_2_js"></spring:url>
		<script src="${ jquery_jvectormap_1_2_2_js}"></script>
		<spring:url value="/resources/ocs/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" var="jquery_jvectormap_js"></spring:url>
		<script src="${jquery_jvectormap_js }"></script>
		<!-- jQuery Knob Chart -->
		<spring:url value="/resources/ocs/plugins/knob/jquery.knob.js" var="jquery_knob_js"></spring:url>
		<script src="${jquery_knob_js }"></script>
		<!-- daterangepicker -->
		<spring:url value="/resources/ocs/dist/js/moment.min.js" var="moment_min_js"></spring:url>
		<script src="${ moment_min_js}"></script>
		<spring:url value="/resources/ocs/plugins/daterangepicker/daterangepicker.js" var="daterangepicker_js"></spring:url>
		<script src="${daterangepicker_js }"></script>
		<!-- datepicker -->
		<spring:url value="/resources/ocs/plugins/datepicker/bootstrap-datepicker.js" var="bootstrap_datepicker_js"></spring:url>
		<script src="${bootstrap_datepicker_js}"></script>
		<!-- Bootstrap WYSIHTML5 -->
		<spring:url value="/resources/ocs/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" var="bootstrap3_wysihtml5_js"></spring:url>
		<script src="${bootstrap3_wysihtml5_js }"></script>
		<!-- Slimscroll -->
		<spring:url value="/resources/ocs/plugins/slimScroll/jquery.slimscroll.min.js" var="jquery_slimscroll_js"></spring:url>
		<script src="${jquery_slimscroll_js }"></script>
		<!-- FastClick -->
		<spring:url value="/resources/ocs/plugins/fastclick/fastclick.js" var="fastclick_js"></spring:url>
		<script src="${fastclick_js }"></script>
		<!-- AdminLTE App -->
		<spring:url value="/resources/ocs/dist/js/app.min.js" var="app_js"></spring:url>
		<script src="${ app_js}"></script>
		<!-- AdminLTE for demo purposes -->
		<spring:url value="/resources/ocs/dist/js/custom.js" var="custom_js"></spring:url>
		<script src="${ custom_js}"></script>
		<spring:url value="/resources/ocs/dist/js/bootstrap-contextmenu.js" var="bootstrap_contextmenu_js"></spring:url>
		<script src="${bootstrap_contextmenu_js }"></script>
		<!-- jstree -->
		<spring:url value="/resources/ocs/jstree/jstree.min.js" var="jstree_js"></spring:url>
		<script src="${ jstree_js}"></script>
		<spring:url value="/resources/ocs/jstree/custom.js" var="jstree_custom_js"></spring:url>
		<script src="${ jstree_custom_js}"></script>
	</head>
  	<body class="hold-transition skin-blue sidebar-mini fixed">
   		<div class="wrapper">
		    <tiles:insertAttribute name="header" ignore="true" />
		    <tiles:insertAttribute name="menu" ignore="true" />
	    	<tiles:insertAttribute name="body"/> 
		    <tiles:insertAttribute name="footer" ignore="true"/>
		</div>
		<script type="text/javascript">
			 $('.datepicker').datepicker({
				autoclose: true,
				todayHighlight: true,
				format: 'dd/mm/yyyy',
				language: 'vn'
			});
		</script> 
	</body>
</html>

