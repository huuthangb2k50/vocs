<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
    <section class="content">
        <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input name="q" class="form-control" placeholder="Search..." type="text">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                    <span  id="context" data-toggle="context" data-target="#context-menu">
                                        Offer Templates
                                    </span>
                                </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                            <span  id="context" data-toggle="context" data-target="#context-menu">
                                                MI Templates
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>

                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                    <!-- Content Header (Page header) -->
                    <!-- InstanceBeginEditable name="EditRegion1" -->
                    <section class="content-header">
                        <h1>Show Billing Cycle Types</h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </section>
                    <section class="content">
                        <div class="box">

                            <!-- /.box-header -->
                            <div class="box-body table-responsive">
                                <page:show id="ps_com_nw_domain_BillingCycleType" object="${billingcycletype}" path="/billingcycletypes" z="z8wlvBgVb+Z7tFIOUGx55XF4KPQ=">
                                    <div class="col-lg-6">
                                    <field:display field="billingCycleTypeName" id="s_com_nw_domain_BillingCycleType_billingCycleTypeName" object="${billingcycletype}" z="5JatNpnH4XHrgeONkcqRRzYU2zY="/>
                                    <field:display field="quantity" id="s_com_nw_domain_BillingCycleType_quantity" object="${billingcycletype}" z="fKfeIZVk6U6d5/KTag1wAmgTgJM="/>
                                    <field:display date="true" dateTimePattern="${billingCycleType_begindate_date_format}" field="beginDate" id="s_com_nw_domain_BillingCycleType_beginDate" object="${billingcycletype}" z="f41FVvsCFWioDfXP2nBBBimUrro="/>
                                    </div>
                                    <div class="col-lg-6">
                                    <field:display field="remark" id="s_com_nw_domain_BillingCycleType_remark" object="${billingcycletype}" z="y35MLoBlm4ceGSfxX0lvzGG4qwU="/>
                                    <field:display field="calcUnitId" id="s_com_nw_domain_BillingCycleType_calcUnitId" object="${billingcycletype}" z="m/4rKTy77owxSJYYzYy0wZEEw5Q="/>
                                    </div>
                                    </page:show>

                            </div>
                        </div>

                    </section>
                </div>
            </div>

        </div>
    </section>
</div>   