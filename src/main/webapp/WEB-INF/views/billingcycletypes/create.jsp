<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
    <section class="content">
        <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input name="q" class="form-control" placeholder="Search..." type="text">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                    <span  id="context" data-toggle="context" data-target="#context-menu">
                                        Offer Templates
                                    </span>
                                </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                            <span  id="context" data-toggle="context" data-target="#context-menu">
                                                MI Templates
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>

                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                    <!-- Content Header (Page header) -->
                    <!-- InstanceBeginEditable name="EditRegion1" -->
                    <section class="content-header">
                        <h1>Create Billing Cycle Types</h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </section>
                    <section class="content">
                        <div class="box">

                            <!-- /.box-header -->
                            <div class="box-body table-responsive">
                                <form:create id="fc_com_nw_domain_BillingCycleType" modelAttribute="billingCycleType" path="/billingcycletypes" render="${empty dependencies}" z="cvEkqcbtZfTz1ppXqpLskRgto6o=">
                                    <div class="col-lg-6">
                                    <field:input field="billingCycleTypeName" id="c_com_nw_domain_BillingCycleType_billingCycleTypeName" z="rJ/aGRKhhv2nhoFXBLoo+daOSbU="/>
                                    <field:input field="quantity" id="c_com_nw_domain_BillingCycleType_quantity" validationMessageCode="field_invalid_integer" z="hxaw5KQIoswABMLDuEQdLQSDsDw="/>
                                    <field:datetime dateTimePattern="${billingCycleType_begindate_date_format}" field="beginDate" id="c_com_nw_domain_BillingCycleType_beginDate" z="9iBJB27o8tCdrf11WUf8smIMr38="/>
                                    </div>
                                    <div class="col-lg-6">
                                    <field:input field="remark" id="c_com_nw_domain_BillingCycleType_remark" z="TJ9jBUph3sxRxlN0o8qNMHrH0a4="/>
                                    <field:select field="calcUnitId" id="c_com_nw_domain_BillingCycleType_calcUnitId" itemValue="calcUnitId" items="${calcunits}" path="/calcunits" z="bntwoeXX+tQnUT+hkTZTMFGr0kg="/>
                                    </div>
                                    </form:create>
                                <form:dependency dependencies="${dependencies}" id="d_com_nw_domain_BillingCycleType" render="${not empty dependencies}" z="JjjiOXVlBnwLnqzfF47FKzTuTJk="/>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->

                    </section>
                    <!-- InstanceEndEditable -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->
</div>   