<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Update triggerEvent</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">triggerEvent</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
   	<form:update id="fu_com_nw_domain_TriggerEvent" idField="triggerEventId" modelAttribute="triggerEvent" path="/triggerevents" versionField="Version" z="KSaiWgbv2Q5Gs6irA22QC5TLMKI=">
        <div class="col-xs-6">
        <field:input field="trigger_event_name" id="c_com_nw_domain_TriggerEvent_trigger_event_name" z="QVqGKk35mYyZRuieVi1Yffl55/U="/>
        <field:input field="status" id="c_com_nw_domain_TriggerEvent_status" validationMessageCode="field_invalid_integer" z="ULTgzBM4xsz/14XfbMp/EoZFW70="/>
        <field:input field="delay_process" id="c_com_nw_domain_TriggerEvent_delay_process" validationMessageCode="field_invalid_integer" z="B1FtQ0s4VEfpQGbFNhV10K08Ers="/>
        <field:select field="destination_id" id="c_com_nw_domain_TriggerEvent_destination_id" itemValue="destinationId" items="${triggerdestinations}" path="/triggerdestinations" z="4HpGRVQshgM73rtPiI2628Y7PFs="/>
        </div>
        <div class="col-xs-6">
        <field:select field="triggerOcsId" id="c_com_nw_domain_TriggerEvent_triggerOcsId" itemValue="triggerOcsId" items="${triggerocss}" path="/triggerocss" required="true" z="otwyBDYYYLYz7W3uHf/nb666d24="/>
        <field:select field="trigger_event_type" id="c_com_nw_domain_TriggerEvent_trigger_event_type" itemValue="trigger_event_type" items="${triggereventtypes}" path="/triggereventtypes" z="ybKj4WdKbdBU8bSR0pg4+lO1VFE="/>
        <field:select field="categoryId" id="c_com_nw_domain_TriggerEvent_categoryId" itemValue="categoryId" items="${categorys}" path="/categorys" z="jKWeGv16d+A4JI3rOvo6oU088ao="/>
    	</div>
    </form:update>
   	
    
    					</div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>   