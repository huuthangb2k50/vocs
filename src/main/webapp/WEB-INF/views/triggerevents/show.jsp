<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Show triggerEvent</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">triggerEvent</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
    <page:show id="ps_com_nw_domain_TriggerEvent" object="${triggerevent}" path="/triggerevents" z="IlnqMYqRhwHObSgNS8aVNPflXts=">
        <div class="col-xs-6">
        <field:display field="trigger_event_name" id="s_com_nw_domain_TriggerEvent_trigger_event_name" object="${triggerevent}" z="LlTZ/lviqQlygy28vDFWbNrUVTg="/>
        <field:display field="status" id="s_com_nw_domain_TriggerEvent_status" object="${triggerevent}" z="rgEXrCnHN5jF0++5eLRags9OVKM="/>
        <field:display field="delay_process" id="s_com_nw_domain_TriggerEvent_delay_process" object="${triggerevent}" z="3RpggXnCTzB3iqfberavvE0Zmmw="/>
        <field:display field="destination_id" id="s_com_nw_domain_TriggerEvent_destination_id" object="${triggerevent}" z="xw9sSBWOBzjFnlqkD/HtcSQp8to="/>
        </div>
        <div class="col-xs-6">
        <field:display field="triggerOcsId" id="s_com_nw_domain_TriggerEvent_triggerOcsId" object="${triggerevent}" z="mW6gxBRq3dbBMQ12vvuh/B6pBZI="/>
        <field:display field="trigger_event_type" id="s_com_nw_domain_TriggerEvent_trigger_event_type" object="${triggerevent}" z="WS40vnD4H8vueFpopTypUl1RKGo="/>
        <field:display field="categoryId" id="s_com_nw_domain_TriggerEvent_categoryId" object="${triggerevent}" z="GXFaMEjsDml1C+BJ8BrcFgX9fRg="/>
    	</div>
    </page:show>
    
    </div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>   