<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>List triggerEvent</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">triggerEvent</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
    <page:list id="pl_com_nw_domain_TriggerEvent" items="${triggerevents}" z="TBgAnwKAuTxxa8XbglhOIPXWAvI=">
        <table:table data="${triggerevents}" id="l_com_nw_domain_TriggerEvent" path="/triggerevents" typeIdFieldName="triggerEventId" z="Na2LD8OTEyWGEEi4a2QEM7xZGec=">
            <table:column id="c_com_nw_domain_TriggerEvent_trigger_event_name" property="trigger_event_name" z="aJLJFlfFg/YGg9QLXjJpgXx6TtY="/>
            <table:column id="c_com_nw_domain_TriggerEvent_status" property="status" z="vY3j4OaaEl3v5teVNeWEC4K91+E="/>
            <table:column id="c_com_nw_domain_TriggerEvent_delay_process" property="delay_process" z="a0hPxuYyMhFNdfmdXhd1SyVg370="/>
            <table:column id="c_com_nw_domain_TriggerEvent_destination_id" property="destination_id" z="mEpVH6IosIyKuw3QfUoFIqullfA="/>
            <table:column id="c_com_nw_domain_TriggerEvent_triggerOcsId" property="triggerOcsId" z="vu96LvxGvuVTH4aaFnTTpE8ZjbM="/>
            <table:column id="c_com_nw_domain_TriggerEvent_trigger_event_type" property="trigger_event_type" z="ZtCmTxKMofzGQimbGrS+1HslvZQ="/>
        </table:table>
    </page:list>
                        </div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>   