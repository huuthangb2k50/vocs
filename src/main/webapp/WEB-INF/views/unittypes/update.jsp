<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Update unitType</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">unitType</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
   	<form:update id="fu_com_nw_domain_UnitType" idField="unitTypeId" modelAttribute="unitType" path="/unittypes" versionField="Version" z="VTXYMlMKbSFi2PWv3JObnBQeBNE=">
        <div class="col-xs-6">
        <field:input field="name" id="c_com_nw_domain_UnitType_name" z="W9HMmc8tlycW2pDJ4mSyO7nLIyk="/>
        <field:input field="precisions" id="c_com_nw_domain_UnitType_precisions" validationMessageCode="field_invalid_integer" z="IYB4bCXsQcl5PsrN/cNJ/esMLco="/>
        <field:input field="baseRate" id="c_com_nw_domain_UnitType_baseRate" validationMessageCode="field_invalid_integer" z="1UeOS7blM/sRSN+upZ4LHHjBGDg="/>
        </div>
        <div class="col-xs-6">
        <field:input field="displayRate" id="c_com_nw_domain_UnitType_displayRate" validationMessageCode="field_invalid_integer" z="jDBVZTd/Xxme4fYKJsQ/5CkVMzI="/>
        <field:input field="remark" id="c_com_nw_domain_UnitType_remark" z="0qIKKOyrFR847fttzISbMJJIlnU="/>
    	</div>
    </form:update>
    
    					</div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>   