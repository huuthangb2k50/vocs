<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Show Zones</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
    <page:show id="ps_com_nw_domain_RateTable" object="${ratetable}" path="/ratetables" z="Bgt0MJ/Pi46Le/G1IFNAYRF4x3U=">
        <field:display field="rateTableName" id="s_com_nw_domain_RateTable_rateTableName" object="${ratetable}" z="8ik222Zuga2fNwFQuwo38iIr7qM="/>
        <field:display field="categoryId" id="s_com_nw_domain_RateTable_categoryId" object="${ratetable}" z="QUTe4Lb8ifiBMyIEaOBf3+zPw74="/>
        <field:display date="true" dateTimePattern="${rateTable_effdate_date_format}" field="effDate" id="s_com_nw_domain_RateTable_effDate" object="${ratetable}" z="YG7V6VN2hEbynLas+iUuY8H0bY4="/>
        <field:display date="true" dateTimePattern="${rateTable_expdate_date_format}" field="expDate" id="s_com_nw_domain_RateTable_expDate" object="${ratetable}" z="PyER4DEVuW/26BjOa8qStN8HSkA="/>
        <field:display field="remark" id="s_com_nw_domain_RateTable_remark" object="${ratetable}" z="yiCRijYIwrK4ILBctQjgrfxFE2Q="/>
        <field:display field="rateTableState" id="s_com_nw_domain_RateTable_rateTableState" object="${ratetable}" z="hu3K9CfJhVKMJ8QaPvcdFmtFaRw="/>
        <field:display field="externalId" id="s_com_nw_domain_RateTable_externalId" object="${ratetable}" z="oAvHNDI+3lCpLpEKkLCXx4QvN6g="/>
        <field:display field="ratingValue" id="s_com_nw_domain_RateTable_ratingValue" object="${ratetable}" z="jhjSps+/8Zac95W8g0DVj7htap8="/>
        <field:display field="formulaId" id="s_com_nw_domain_RateTable_formulaId" object="${ratetable}" z="/BqQ9qVYil0vY67SMeziz+3cbZs="/>
        <field:display field="defaultFormulaId" id="s_com_nw_domain_RateTable_defaultFormulaId" object="${ratetable}" z="GNwanq6X6u9iv9vOJy+/TB+xMUk="/>
        <field:display field="decisionTableId" id="s_com_nw_domain_RateTable_decisionTableId" object="${ratetable}" z="ceQQDsS2OMSgXLJU05nC27BXWVk="/>
    </page:show>
    
    </div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>   