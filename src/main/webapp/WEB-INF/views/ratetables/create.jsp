<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Create RateTable</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
    <form:create id="fc_com_nw_domain_RateTable" modelAttribute="rateTable" path="/ratetables" render="${empty dependencies}" z="QopfDgaqBsDtUVcUFiz2Zho4x/c=">
        <div class="col-xs-6">
        <field:input field="rateTableName" id="c_com_nw_domain_RateTable_rateTableName" z="7UYNqZYSB+/L0/flAc0d6xOfP8A="/>
        <field:select field="categoryId" id="c_com_nw_domain_RateTable_categoryId" itemValue="categoryId" items="${categorys}" path="/categorys" z="OI+WzcKiUi/U++v3mQaKDoLqHy0="/>
        <field:datetime dateTimePattern="${rateTable_effdate_date_format}" field="effDate" id="c_com_nw_domain_RateTable_effDate" z="m2H6s2ha34kLlbOebsKsiFTSHKg="/>
        <field:datetime dateTimePattern="${rateTable_expdate_date_format}" field="expDate" id="c_com_nw_domain_RateTable_expDate" z="b8Ak8SrMwfkslX05a8TLeR5WGI8="/>
        <field:input field="remark" id="c_com_nw_domain_RateTable_remark" z="JwE+e7rRb2B6es+ahLlUHkSrRHk="/>
        <field:input field="rateTableState" id="c_com_nw_domain_RateTable_rateTableState" validationMessageCode="field_invalid_integer" z="sF/qMcWGkhrA5YUReSNAT4mAIuQ="/>
        </div>
        <div class="col-xs-6">
        <field:input field="externalId" id="c_com_nw_domain_RateTable_externalId" validationMessageCode="field_invalid_integer" z="6QPIOVs9vX4EAMzVRvyasqfulGo="/>
        <field:input field="ratingValue" id="c_com_nw_domain_RateTable_ratingValue" validationMessageCode="field_invalid_integer" z="xhw1ySdPAcj1zRz4vm7XoTqJ8Mk="/>
        <field:input field="formulaId" id="c_com_nw_domain_RateTable_formulaId" validationMessageCode="field_invalid_integer" z="OdwkSIR9f3khsXI9LRgVynr+TBU="/>
        <field:input field="defaultFormulaId" id="c_com_nw_domain_RateTable_defaultFormulaId" validationMessageCode="field_invalid_integer" z="AFjO3k+hwvP+1JQJMMdS+m1Up9U="/>
        <field:select field="decisionTableId" id="c_com_nw_domain_RateTable_decisionTableId" itemValue="decisionTableId" items="${decisiontables}" path="/decisiontables" z="t+SCs4HOxUznMJcYEA4NbxI54pU="/>
    	</div>
    </form:create>
    <form:dependency dependencies="${dependencies}" id="d_com_nw_domain_RateTable" render="${not empty dependencies}" z="ELCb7APoMRYqhxza1LPMY7wS8dc="/>
                        </div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>   