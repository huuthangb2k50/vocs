<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Rate Table for Purchase</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
                <section class="content">
                	<div class="well well-sm fillter">
                    	<form class="form-inline">
                            <div class="form-group">
                                <input class="form-control" id="exampleInputEmail3" placeholder="Threshold name" type="text">
                            </div>
                            <div class="form-group">
                                <select class="form-control">
                                        <option value="0">Threshold Type</option>
                                        <option value="1">Online</option>
                                        <option value="2">Đang xử lý</option>
                                        <option value="3">Bị lỗi</option>
                                    </select>
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Fillter</button>
                            <a class="btn btn-default pull-right" data-toggle="modal" href="#Addnew10"><i class="fa fa-plus"></i> Add new</a>
                        </form>
                    </div>
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
    <page:list id="pl_com_nw_domain_RateTable" items="${ratetables}" z="dtN4hiH7Uxs9ZAsEbsPKeBVQzqg=">
        <table:table data="${ratetables}" id="l_com_nw_domain_RateTable" path="/ratetables" typeIdFieldName="rateTableId" z="CnmgehQMEWOB5dKxht3h5fBKpPs=">
            <table:column id="c_com_nw_domain_RateTable_rateTableName" property="rateTableName" z="yB3ZvshOP1wicYqJGNFqcKCg/Gw="/>
            <table:column id="c_com_nw_domain_RateTable_categoryId" property="categoryId" z="QvZB7SeW6loP418zqpjZtRR9K1g="/>
            <table:column date="true" dateTimePattern="${rateTable_effdate_date_format}" id="c_com_nw_domain_RateTable_effDate" property="effDate" z="4EkPkwLJT3fgJQ+4Q8gAng9DZzk="/>
            <table:column date="true" dateTimePattern="${rateTable_expdate_date_format}" id="c_com_nw_domain_RateTable_expDate" property="expDate" z="HnhcG6m0pe4641u0y7TGAUIEU7A="/>
            <table:column id="c_com_nw_domain_RateTable_remark" property="remark" z="BQvIlf91DH7gq7HbcgNvQE9Dppg="/>
            <table:column id="c_com_nw_domain_RateTable_rateTableState" property="rateTableState" z="+UisVYwbrSjYl+NEXm7/JcgRWzU="/>
        </table:table>
    </page:list>
                        </div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>   