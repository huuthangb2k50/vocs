<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
    <section class="content">
        <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input name="q" class="form-control" placeholder="Search..." type="text">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                    <span  id="context" data-toggle="context" data-target="#context-menu">
                                        Offer Templates
                                    </span>
                                </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                            <span  id="context" data-toggle="context" data-target="#context-menu">
                                                MI Templates
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>

                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                    <!-- Content Header (Page header) -->
                    <!-- InstanceBeginEditable name="EditRegion1" -->
                    <section class="content-header">
                        <h1>Show BalTypes</h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </section>
                    <section class="content">
                        <div class="box">

                            <!-- /.box-header -->
                            <div class="box-body table-responsive">
                                <page:show id="ps_com_nw_domain_BalType" object="${baltype}" path="/baltypes" z="BqiyBWaexfRN4tKdYfOgsHFGjx0=">
                                    <div class="col-lg-6">
                                    <field:display field="balTypeName" id="s_com_nw_domain_BalType_balTypeName" object="${baltype}" z="yyLNzwgoc1gvmp2L1MJy4iF0vRQ="/>
                                    <field:display field="description" id="s_com_nw_domain_BalType_description" object="${baltype}" z="K2ZNUIRcDSnNDgke27FW6jxm/Vw="/>
                                    <field:display field="externalId" id="s_com_nw_domain_BalType_externalId" object="${baltype}" z="K6k1ZRzt9ymBoWyvl2BaIBmHnRw="/>
                                    <field:display field="balTypeType" id="s_com_nw_domain_BalType_balTypeType" object="${baltype}" z="9FO9oslI546DayMPejv7jajNQbM="/>
                                    <field:display field="categoryId" id="s_com_nw_domain_BalType_categoryId" object="${baltype}" z="x/OMDmAl4CKvsYq6z+jl89VrjUo="/>
                                    <field:display field="paymentType" id="s_com_nw_domain_BalType_paymentType" object="${baltype}" z="S2+RTyuMjjRm+gjLnJUXZMr3M7Q="/>
                                    <field:display field="isCurrency" id="s_com_nw_domain_BalType_isCurrency" object="${baltype}" z="jpQkl5A0ZxcT4ZeA3eH6Qmqcrb8="/>
                                    <field:display field="percision" id="s_com_nw_domain_BalType_percision" object="${baltype}" z="Qfcu9oLHvrLHyVJBTf64L/HqHmk="/>
                                    <field:display field="effDateType" id="s_com_nw_domain_BalType_effDateType" object="${baltype}" z="7yG8E03cZdc8dCJsxfZIJl1d4yI="/>
                                    
                                    <field:display date="true" dateTimePattern="${balType_effdate_date_format}" field="effDate" id="s_com_nw_domain_BalType_effDate" object="${baltype}" z="5ADzSa3H4ZeNSKUDIHhzYR3FRSs="/>
                                    <field:display field="expDateType" id="s_com_nw_domain_BalType_expDateType" object="${baltype}" z="cfwZ53Zs9+Ih5I2kfKxjsAQpn/g="/>
                                    </div>
                                    <div class="col-lg-6">
                                    <field:display date="true" dateTimePattern="${balType_expdate_date_format}" field="expDate" id="s_com_nw_domain_BalType_expDate" object="${baltype}" z="0YqqnXURZ8C0Gl01Dp6SPVSecGY="/>
                                    <field:display field="recurringType" id="s_com_nw_domain_BalType_recurringType" object="${baltype}" z="pvfvV2S0E71p5RmmHMpVmA3JlG0="/>
                                    <field:display field="recurringPeriod" id="s_com_nw_domain_BalType_recurringPeriod" object="${baltype}" z="SQIO51KE1TCGylzo0vIhuYBL294="/>
                                    <field:display field="isAcm" id="s_com_nw_domain_BalType_isAcm" object="${baltype}" z="umiKTwleuRmGNGSgXU7Ty3ZcZRY="/>
                                    <field:display field="unitTypeId" id="s_com_nw_domain_BalType_unitTypeId" object="${baltype}" z="jCankGETblD+D6dLyqADymkyjI0="/>
                                    <field:display field="periodicPeriodType" id="s_com_nw_domain_BalType_periodicPeriodType" object="${baltype}" z="6FqW800mAhoFrezw8lTSZBswDAU="/>
                                    <field:display field="periodicPeriod" id="s_com_nw_domain_BalType_periodicPeriod" object="${baltype}" z="SMYq0GIWky2oAlHYHBb0WS0aKIs="/>
                                    <field:display field="windowSize" id="s_com_nw_domain_BalType_windowSize" object="${baltype}" z="n7kW03PjhDg8H5LvHVO1Ui7I1LE="/>
                                    <field:display field="lowWaterMarkLevel" id="s_com_nw_domain_BalType_lowWaterMarkLevel" object="${baltype}" z="dszokXZnl/l4Tu/MQ5a6MqEduSE="/>
                                    <field:display field="highWatermarkLevel" id="s_com_nw_domain_BalType_highWatermarkLevel" object="${baltype}" z="6dF6HXxD4Ryg8RBizh5Too6px0A="/>
                                    <field:display field="billingCycleTypeId" id="s_com_nw_domain_BalType_billingCycleTypeId" object="${baltype}" z="Gwjxmf9WQ2Kh+twYIPq5a58LVl8="/>
                                    </div>
                                    </page:show>

                            </div>
                        </div>

                    </section>
                </div>
            </div>

        </div>
    </section>
</div>   
