<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
    <section class="content">
        <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input name="q" class="form-control" placeholder="Search..." type="text">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                    <span  id="context" data-toggle="context" data-target="#context-menu">
                                        Offer Templates
                                    </span>
                                </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                            <span  id="context" data-toggle="context" data-target="#context-menu">
                                                MI Templates
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>

                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                    <!-- Content Header (Page header) -->
                    <!-- InstanceBeginEditable name="EditRegion1" -->
                    <section class="content-header">
                        <h1>List Balance Types</h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                            <li class="active">List Balance Types</li>
                        </ol>
                    </section>
                    <section class="content">
                        <div class="box">

                            <!-- /.box-header -->
                            <div class="box-body table-responsive">
                                <page:list id="pl_com_nw_domain_BalType" items="${baltypes}" z="XaLklXlcpnDASAwck3quHoGJd1c=">
                                    <table:table data="${baltypes}" id="l_com_nw_domain_BalType" path="/baltypes" typeIdFieldName="balTypeId" z="clc0nyglFkB0PwAnj/YmUzmiAwA=">
                                        <table:column id="c_com_nw_domain_BalType_balTypeName" property="balTypeName" z="3ZjW89Z5HGnr3N93abMzpRf2qik="/>
                                        <table:column id="c_com_nw_domain_BalType_description" property="description" z="pgZGj4wEl0jszTkJEWiUYe3svSQ="/>
                                        <table:column id="c_com_nw_domain_BalType_externalId" property="externalId" z="hOthdVK/v6sJ9iI3VzHxLKuKXdk="/>
                                        <table:column id="c_com_nw_domain_BalType_balTypeType" property="balTypeType" z="7ATZgzE/PgXmqnEwbyYzb/JdiMQ="/>
                                        <table:column id="c_com_nw_domain_BalType_categoryId" property="categoryId" z="DdMaiMZx4jbHiht6w0CTJ79PdDA="/>
                                        <table:column id="c_com_nw_domain_BalType_paymentType" property="paymentType" z="C+i870O6zljv5gmuOAs3ht2ZuWo="/>
                                    </table:table>
                                </page:list>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->

                    </section>
                    <!-- InstanceEndEditable -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->
</div>  