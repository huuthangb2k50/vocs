<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
    <section class="content">
        <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input name="q" class="form-control" placeholder="Search..." type="text">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                    <span  id="context" data-toggle="context" data-target="#context-menu">
                                        Offer Templates
                                    </span>
                                </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                            <span  id="context" data-toggle="context" data-target="#context-menu">
                                                MI Templates
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>

                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                    <!-- Content Header (Page header) -->
                    <!-- InstanceBeginEditable name="EditRegion1" -->
                    <section class="content-header">
                        <h1>Update BalTypes</h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                            <li class="active">BalTypes</li>
                        </ol>
                    </section>
                    <section class="content">
                        <div class="box">

                            <!-- /.box-header -->
                            <div class="box-body table-responsive">
                                <form:update id="fu_com_nw_domain_BalType" idField="balTypeId" modelAttribute="balType" path="/baltypes" versionField="Version" z="SrbD6S2XRhSR2fCyB02I2rqEpyA=">
                                    <div class="col-md-6">
                                        <field:input field="balTypeName" id="c_com_nw_domain_BalType_balTypeName" z="94EwjyugGyYEsJKgivFg8lqybiw="/>
                                        <field:input field="description" id="c_com_nw_domain_BalType_description" z="V83lhU/feavTF0DsUiHHtKX7ISo="/>
                                        <field:input field="externalId" id="c_com_nw_domain_BalType_externalId" validationMessageCode="field_invalid_integer" z="hVpKPjGcSmngso6ZwV8tIXuS0ko="/>
                                        <field:input field="balTypeType" id="c_com_nw_domain_BalType_balTypeType" validationMessageCode="field_invalid_integer" z="TmZnkt4umd6IqpWC2eeHBLYwKwE="/>
                                        <field:input field="categoryId" id="c_com_nw_domain_BalType_categoryId" validationMessageCode="field_invalid_integer" z="I/vLwK/umMLiec7liZQdufWGlnA="/>
                                        <field:input field="paymentType" id="c_com_nw_domain_BalType_paymentType" z="xQYRXUSMvz2k0WX15ZcZppzSvfM="/>
                                        <field:input field="isCurrency" id="c_com_nw_domain_BalType_isCurrency" z="0CNJ53vPDadtYSwQBJ9ICxahynk="/>
                                        <field:input field="percision" id="c_com_nw_domain_BalType_percision" validationMessageCode="field_invalid_integer" z="4lARH6CeAGYBrtWgVeuQR1+hX6M="/>
                                        <field:input field="effDateType" id="c_com_nw_domain_BalType_effDateType" validationMessageCode="field_invalid_integer" z="zKuiEc3bZiaOKqZ9ysfpxB5nl7U="/>
                                    
                                        <field:datetime dateTimePattern="${balType_effdate_date_format}" field="effDate" id="c_com_nw_domain_BalType_effDate" z="rmi0CZDYOCN8/adzAW+zazbhDS0="/>
                                        <field:input field="expDateType" id="c_com_nw_domain_BalType_expDateType" validationMessageCode="field_invalid_integer" z="a8dmCEy92UPGtl/jy5gbFIRMrQM="/>
                                    </div>
                                    <div class="col-md-6">
                                        <field:datetime dateTimePattern="${balType_expdate_date_format}" field="expDate" id="c_com_nw_domain_BalType_expDate" z="xeyQqGIJgdpa6bNTZ5edz+MOksU="/>
                                        <field:input field="recurringType" id="c_com_nw_domain_BalType_recurringType" validationMessageCode="field_invalid_integer" z="IAkGCAFUE6a2SNau4oKoRzpEKTk="/>
                                        <field:input field="recurringPeriod" id="c_com_nw_domain_BalType_recurringPeriod" validationMessageCode="field_invalid_integer" z="jH8DMafpqyakdC/V4u3JCuxrL+g="/>
                                        <field:checkbox field="isAcm" id="c_com_nw_domain_BalType_isAcm" z="znit/y870wqhRlp5hspE+AOUWXE="/>
                                        <field:select field="unitTypeId" id="c_com_nw_domain_BalType_unitTypeId" itemValue="unitTypeId" items="${unittypes}" path="/unittypes" z="j+qFrXoZ/KtotszV7y6H1fTo3kA="/>
                                        <field:input field="periodicPeriodType" id="c_com_nw_domain_BalType_periodicPeriodType" validationMessageCode="field_invalid_integer" z="RH3KcKNw52dDfE+N9sXmjmMxppw="/>
                                        <field:input field="periodicPeriod" id="c_com_nw_domain_BalType_periodicPeriod" validationMessageCode="field_invalid_integer" z="z4/nutD+mtFQ4a+SxQfhnmMTcx0="/>
                                        <field:input field="windowSize" id="c_com_nw_domain_BalType_windowSize" validationMessageCode="field_invalid_integer" z="nkWA5kvkc3hlwTonibgrRJNdj4U="/>
                                        <field:input field="lowWaterMarkLevel" id="c_com_nw_domain_BalType_lowWaterMarkLevel" validationMessageCode="field_invalid_integer" z="Qv/jf+auzeY6qKrtIGnco+AErAk="/>
                                        <field:input field="highWatermarkLevel" id="c_com_nw_domain_BalType_highWatermarkLevel" validationMessageCode="field_invalid_integer" z="txFjxS3mbvMngUJsUcsv5rPBwqY="/>
                                        <field:select field="billingCycleTypeId" id="c_com_nw_domain_BalType_billingCycleTypeId" itemValue="billingCycleTypeId" items="${billingcycletypes}" path="/billingcycletypes" z="NehzFmypAJoQ6nk1nzAvxZq+Sfw="/>
                                    </div>
                                </form:update>

								<page:list id="pl_com_nw_domain_ThresholdBalTypeMap" items="${result}" z="xPPGgKyRoCcaUf2ix04ab9S4cWY=">
							        <%-- <table:table data="${result}" id="l_com_nw_domain_ThresholdBalTypeMap" path="/thresholdbaltypemaps" typeIdFieldName="thresholdBaltypeMapId" z="sBXhcSIKwpiK768Y87se37VjYzw=">
							            <table:column id="c_com_nw_domain_ThresholdBalTypeMap_thresholdId" property="thresholdId" z="Oxv4o8rVg74DknzwCQByRnBIKQE="/>
							            <table:column id="c_com_nw_domain_ThresholdBalTypeMap_thresholdId" property=" "></table:column>
							            
							            <table:column id="c_com_nw_domain_ThresholdBalTypeMap_baltypeId" property="baltypeId" z="nbnuKJjWT8waKRvRn3e1atnrLC0="/>
							        </table:table> --%>
								        <div class="box-body">
											<table class="table table-bordered">
												<thead>
													<tr>
														<th>Threshold Id</th>
														<th>Threshold Name</th>
														<th>Type</th>
														<th>Value</th>
														<th>External ID</th>
														<th colspan="3" >Action</th>
													</tr>
												</thead>
												<tbody>
												<c:forEach items="${result}" var="resultTmp">
													<tr>
														<td>${resultTmp.thresholdId.thresholdId}</td>
														<td>${resultTmp.thresholdId.thresholdName}</td>
														<td>${resultTmp.thresholdId.thresholdType}</td>
														<td>${resultTmp.thresholdId.thresValue}</td>
														<td>${resultTmp.thresholdId.externalId}</td>
														<spring:url value="/resources/images/show.png" var="show"></spring:url>
														<td class="utilbox"><a href="thresholdbaltypemaps/${resultTmp.thresholdBaltypeMapId}"><img src="${show }" class="image" ></a></td>
														<spring:url value="/resources/images/update.png" var="update"></spring:url>
														<td class="utilbox"><a href="thresholdbaltypemaps/${resultTmp.thresholdBaltypeMapId}?form"><img src="${update }" class="image"></a></td>
														<spring:url value="/resources/images/delete.png" var="delete"></spring:url>
														<td class="utilbox">
															<form id="command" action="thresholdbaltypemaps/${resultTmp.thresholdBaltypeMapId}" method="post">
																<input name="_method" value="DELETE" type="hidden">
																<input onclick="return confirm('Are you sure want to delete this item?');" src="${delete }" class="image" type="image">
															</form>
														</td>
													</tr>
												</c:forEach>
													<tr class="footer">
														<td colspan="7"></td>
														<spring:url value="/resources/images/add.png" var="add"></spring:url>
														<td colspan="1"><span class="new"><a href="thresholdbaltypemaps?form"><img src="${add }" ></a></span> </td>
														
													</tr>
												</tbody>
											</table>
												
										</div>
							        
							    </page:list>
							
                            </div>
                        </div>

                    </section>
                </div>
            </div>

        </div>
    </section>
</div>   