<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Create triggerFieldList</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">triggerFieldList</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
   	<form:create id="fc_com_nw_domain_TriggerFieldList" modelAttribute="triggerFieldList" path="/triggerfieldlists" render="${empty dependencies}" z="Oj8nVdnkt3isR+Jiad/wc1RzsAs=">
        <div class="col-xs-6">
        <field:input field="fieldName" id="c_com_nw_domain_TriggerFieldList_fieldName" required="true" z="d6z0gKEuvMqgBgJKjIM+bRonHfU="/>
        <field:input field="fieldOrder" id="c_com_nw_domain_TriggerFieldList_fieldOrder" validationMessageCode="field_invalid_integer" z="4jfziKRJ8RU8mjlNxbca8oTtpmA="/>
        </div>
        <div class="col-xs-6">
        <field:input field="fieldFormat" id="c_com_nw_domain_TriggerFieldList_fieldFormat" validationMessageCode="field_invalid_integer" z="SjfQdAEj5phNX29izz69W5vtIS0="/>
        <field:select field="fieldBuilderId" id="c_com_nw_domain_TriggerFieldList_fieldBuilderId" itemValue="fieldBuilderId" items="${triggerfieldbuilders}" path="/triggerfieldbuilders" z="oX6wCdBQiLfjvVgQrXBuJh7ORfo="/>
    	</div>
    </form:create>
    <form:dependency dependencies="${dependencies}" id="d_com_nw_domain_TriggerFieldList" render="${not empty dependencies}" z="MvFKHfhQ812kthVMVM/sdgKpdTY="/>
</div>
    
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>   