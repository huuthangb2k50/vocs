<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Rate Table for Purchase</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
                <section class="content">
                	<div class="well well-sm fillter">
                    	<form class="form-inline">
                            <div class="form-group">
                                <input class="form-control" id="exampleInputEmail3" placeholder="Threshold name" type="text">
                            </div>
                            <div class="form-group">
                                <select class="form-control">
                                        <option value="0">Threshold Type</option>
                                        <option value="1">Online</option>
                                        <option value="2">Đang xử lý</option>
                                        <option value="3">Bị lỗi</option>
                                    </select>
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Fillter</button>
                            <a class="btn btn-default pull-right" data-toggle="modal" href="#Addnew10"><i class="fa fa-plus"></i> Add new</a>
                        </form>
                    </div>
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
    <page:list id="pl_com_nw_domain_Formula" items="${formulas}" z="zVptHcYeSH5dxicZ2vcg0q+oKOw=">
        <table:table data="${formulas}" id="l_com_nw_domain_Formula" path="/formulas" typeIdFieldName="formulaId" z="WBxrYkAxY243jZHlPuxJ6WNoVvU=">
            <table:column id="c_com_nw_domain_Formula_formulaType" property="formulaType" z="wmyeygnd3V3QCWlW2B7vE8NH+qI="/>
            <table:column id="c_com_nw_domain_Formula_a" property="a" z="xIObrf+lUkPz41nXwK9ofLHqTDY="/>
            <table:column id="c_com_nw_domain_Formula_b" property="b" z="2Qle6RDJuhpt6oE2yhc6lQGPWFE="/>
            <table:column id="c_com_nw_domain_Formula_per" property="per" z="+6r3S3fCyqLpMxTED1xsA3IItmE="/>
            <table:column id="c_com_nw_domain_Formula_categoryId" property="categoryId" z="nHN0jWwCfs/gkKC1XbZiYDjOdsk="/>
            <table:column id="c_com_nw_domain_Formula_triggerOcsId" property="triggerOcsId" z="t8Kh5DaMYTmppmVrji8wexXFN5k="/>
        </table:table>
    </page:list>
                        </div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>   