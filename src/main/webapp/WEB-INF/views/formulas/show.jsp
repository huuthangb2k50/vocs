<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Show Zones</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
    <page:show id="ps_com_nw_domain_Formula" object="${formula}" path="/formulas" z="0GYVr4JX+f1gGJ3slKf7kamZV1c=">
        <field:display field="formulaType" id="s_com_nw_domain_Formula_formulaType" object="${formula}" z="PQ3JQ49+3T6+6J5ucSRoPw2eA7U="/>
        <field:display field="a" id="s_com_nw_domain_Formula_a" object="${formula}" z="MKKjtK4slytXJnWirYU3b0JQ/qQ="/>
        <field:display field="b" id="s_com_nw_domain_Formula_b" object="${formula}" z="TSsE3yJrbuE/V7KMlvUjPenwRNU="/>
        <field:display field="per" id="s_com_nw_domain_Formula_per" object="${formula}" z="SUZNsVh7ea4uz2KrlUr0oMicdjI="/>
        <field:display field="categoryId" id="s_com_nw_domain_Formula_categoryId" object="${formula}" z="2vJvO4BrOPdvQY+3OHQYnuzxmx8="/>
        <field:display field="triggerOcsId" id="s_com_nw_domain_Formula_triggerOcsId" object="${formula}" z="wkhJP4dgmwqMEU0GnKlBZew2ftI="/>
        <field:display field="isPercentage" id="s_com_nw_domain_Formula_isPercentage" object="${formula}" z="AYMREcxl4W8++zmULTlRq0/hDVY="/>
        <field:display field="templateBits" id="s_com_nw_domain_Formula_templateBits" object="${formula}" z="vQUlIPv1vaYdn31HAi4jDsIjOuA="/>
        <field:display field="normalizingValueType" id="s_com_nw_domain_Formula_normalizingValueType" object="${formula}" z="K0eM4hms3PDjg4JUJ1PNFlrNRWM="/>
        <field:display field="formulaErrorCode" id="s_com_nw_domain_Formula_formulaErrorCode" object="${formula}" z="HBuBLdNopNapSUo6ldXCjrVHz1g="/>
    </page:show>
    
    </div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>   