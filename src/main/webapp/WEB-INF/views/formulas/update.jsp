<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Update Zones</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
    <form:update id="fu_com_nw_domain_Formula" idField="formulaId" modelAttribute="formula" path="/formulas" versionField="Version" z="zJ0YknPojDcxHQePt4dBHw930No=">
        <field:input field="formulaType" id="c_com_nw_domain_Formula_formulaType" validationMessageCode="field_invalid_integer" z="2Uf1s8aK2VDiQZ9sm5Zcdi2E2ww="/>
        <field:input field="a" id="c_com_nw_domain_Formula_a" validationMessageCode="field_invalid_integer" z="Sf9qp9TcqH3KixGEbXQtK8tB2JY="/>
        <field:input field="b" id="c_com_nw_domain_Formula_b" validationMessageCode="field_invalid_integer" z="tLXZVv7uiLscvHzj6ZlBiRUoEvs="/>
        <field:input field="per" id="c_com_nw_domain_Formula_per" validationMessageCode="field_invalid_integer" z="viKC5cthuK0AmByftsVPYR3lj9E="/>
        <field:select field="categoryId" id="c_com_nw_domain_Formula_categoryId" itemValue="categoryId" items="${categorys}" path="/categorys" z="aYY5McKADEjV4DEN4wSrTjo/mro="/>
        <field:select field="triggerOcsId" id="c_com_nw_domain_Formula_triggerOcsId" itemValue="triggerOcsId" items="${triggerocss}" path="/triggerocss" z="X3k1MOc4S0duQUE4V+BxVHleiiY="/>
        <field:checkbox field="isPercentage" id="c_com_nw_domain_Formula_isPercentage" z="5QG0SDKXHR2+rQ2+IkBg02Y98Ec="/>
        <field:input field="templateBits" id="c_com_nw_domain_Formula_templateBits" validationMessageCode="field_invalid_integer" z="ALZcSDWffZ6UxRj78FkIV4Qibe4="/>
        <field:input field="normalizingValueType" id="c_com_nw_domain_Formula_normalizingValueType" validationMessageCode="field_invalid_integer" z="TewqtxmDkPWbJ5SvYvWn+CxbhU0="/>
        <field:input field="formulaErrorCode" id="c_com_nw_domain_Formula_formulaErrorCode" validationMessageCode="field_invalid_integer" z="Jc6J/wHWgU9WMbkcj5bZjrQK/Ss="/>
    </form:update>
    
    					</div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>   