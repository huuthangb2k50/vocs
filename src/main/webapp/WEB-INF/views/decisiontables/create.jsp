<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
    <section class="content">
        <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input name="q" class="form-control" placeholder="Search..." type="text">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                    <span  id="context" data-toggle="context" data-target="#context-menu">
                                        Offer Templates
                                    </span>
                                </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                            <span  id="context" data-toggle="context" data-target="#context-menu">
                                                MI Templates
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>

                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                    <!-- Content Header (Page header) -->
                    <!-- InstanceBeginEditable name="EditRegion1" -->
                    <section class="content-header">
                        <h1>Create Decision Tables</h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </section>
                    <section class="content">
                        <div class="box">

                            <!-- /.box-header -->
                            <div class="box-body table-responsive">
                                <form:create id="fc_com_nw_domain_DecisionTable" modelAttribute="decisionTable" path="/decisiontables" render="${empty dependencies}" z="NMVppkeb6n57PiZvMG1p/jPxX70=">
                                    <div class="col-lg-6">
                                    <field:input field="decisionTableName" id="c_com_nw_domain_DecisionTable_decisionTableName" z="3j8DLLj93LufXPDARLNoSPAIhcg="/>
                                    <field:input field="defaultFormulaIndex" id="c_com_nw_domain_DecisionTable_defaultFormulaIndex" validationMessageCode="field_invalid_integer" z="zU5WIPCmevQbLndO+xdwV42L4+0="/>
                                    </div>
                                    <div class="col-lg-6">
                                    <field:input field="remark" id="c_com_nw_domain_DecisionTable_remark" z="usS9wv3ywWfJ59bPnmoPK3x39KM="/>
                                    <field:select field="categoryId" id="c_com_nw_domain_DecisionTable_categoryId" itemValue="categoryId" items="${categorys}" path="/categorys" z="FyrJOHG+MG9764rNfnm6tSVqCBs="/>
                                    </div>
                                </form:create>
                                <form:dependency dependencies="${dependencies}" id="d_com_nw_domain_DecisionTable" render="${not empty dependencies}" z="mWLzFT4w01NVisT1kaMADaasbuo="/>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->

                    </section>
                    <!-- InstanceEndEditable -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->
</div>   