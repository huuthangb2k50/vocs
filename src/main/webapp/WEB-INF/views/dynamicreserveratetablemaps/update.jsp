<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
    <section class="content">
        <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input name="q" class="form-control" placeholder="Search..." type="text">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                    <span  id="context" data-toggle="context" data-target="#context-menu">
                                        Offer Templates
                                    </span>
                                </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                            <span  id="context" data-toggle="context" data-target="#context-menu">
                                                MI Templates
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>

                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                    <!-- Content Header (Page header) -->
                    <!-- InstanceBeginEditable name="EditRegion1" -->
                    <section class="content-header">
                        <h1>Update DynamicReserveRateTableMaps</h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </section>
                    <section class="content">
                        <div class="box">

                            <!-- /.box-header -->
                            <div class="box-body table-responsive">
                                <form:update id="fu_com_nw_domain_DynamicReserveRateTableMap" idField="dynamicRateMapId" modelAttribute="dynamicReserveRateTableMap" path="/dynamicreserveratetablemaps" versionField="Version" z="Q3EIqty95UbQXwe530Ar6eJ5xm8=">
                                    <field:select field="rateTableId" id="c_com_nw_domain_DynamicReserveRateTableMap_rateTableId" itemValue="rateTableId" items="${ratetables}" path="/ratetables" z="0wrBOqJp65zoa31Y1R86Ji55YkA="/>
                                    <field:select field="dynamicReserveId" id="c_com_nw_domain_DynamicReserveRateTableMap_dynamicReserveId" itemValue="dynamicReserveId" items="${dynamicreserves}" path="/dynamicreserves" z="MXj7odKfyp5JS61iPOz/dBS1dok="/>
                                    <field:input field="rateTableIndex" id="c_com_nw_domain_DynamicReserveRateTableMap_rateTableIndex" validationMessageCode="field_invalid_integer" z="KJMLMSoGDwDgueSMrtGEYPm6bWA="/>
                                </form:update>

                            </div>
                        </div>

                    </section>
                </div>
            </div>

        </div>
    </section>
</div>   