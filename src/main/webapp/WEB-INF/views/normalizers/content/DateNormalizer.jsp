<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

 
    	
    		<div class="col-md-12">
    			<div class="col-md-3">
					<input type="checkbox" id="curingtime" onchange="processCurringTime()">
					<label class="control-label" >IsCuringTimeUsing</label>
				</div>
				<div class="col-md-2">
					<input type="checkbox" id="static" onchange="processStatic()">
					<label class="control-label">IsStaticInput</label>
				</div>
				
				<div class="col-md-7 form-group" id="c_com_nw_domain_Normalizer_specialFields">
	    			<div class="col-md-3"><label class=" control-label" for="specialFields">Special Fields</label></div>
	    			<div class="col-md-9"><input id="specialFields" name="specialFields" class="form-control" value="" type="text"></div>
	    		</div>
				
			</div>
			<div class="col-md-12">
    		<div class="form-group" id="_c_com_nw_domain_Normalizer_inputField_id">
    			<label class="col-sm-2 control-label" for="_inputField_id">Input Field</label>
    			<div class="col-sm-10"><input id="_inputField_id" name="inputField" class="form-control" value="" type="text"><br></div>
    		</div>
    		</div>
    	<div class="col-md-12" id="hideshow">
    		
    		<div class="col-md-2">
    			<input type="button" class="form-control btn bg-gray" value="Add Child" onclick="AddChild();" />
    		</div>
    		<div class="col-md-10">
				<table class="table table-bordered" id="ListChild">
					<thead>
						<tr>
							<th class="bg-blue col-md-4">Path/ChildPath</th>
							<th class="bg-blue col-md-4">Filter</th>
							<th class="bg-blue col-md-4">Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<select class="form-control" data-toggle="modal" data-target="#ListPath">
					    			<option value="">Path/ChildPath</option>
					    			<option value="01">Path/ChildPath 01</option>
					    		</select>
    						</td>
							<td>
								<select class="form-control">
					    			<option value="">Filter</option>
					    			<option value="01">Filter 01</option>
					    		</select>
							</td>
							<td>
								Edit/Delete
							</td>
						</tr>
					</tbody>
				</table>
												
			</div>
    		
    	</div>
    	<div class="col-md-12">
    		
    		<div class="form-group" id="_c_com_nw_domain_Normalizer_defaultValue_id">
    			<label class="col-sm-2 control-label" for="_defaultValue_id">Default Value</label>
    			<div class="col-sm-4">
    			
	    			<select id="_defaultValue_id" name="defaultValue" class="form-control">
		                <option value=""></option>
		                
		            </select>
	    			<br>
    			</div>
    		</div>
    	</div>
    	
    	<div class="col-md-12">
    		<div class="col-md-2">
    			<input type="button" class="form-control btn bg-gray " value="Edit Values" data-toggle="modal" data-target="#ListValue" />
    		</div>
    		<div class="col-md-2">
    			<input type="button" class="form-control btn bg-gray " value="Add New Parameter" onclick="AddParameter();" />
    		</div>
    		<div class="col-md-2">
    			<input type="button" class="form-control btn bg-gray " value="Validate" onclick="" /><br />
    		</div>

    	</div>
    	<div class="col-md-12 " style="padding-top: 20px;">
    	<div class="col-md-12">
				<table class="table table-bordered" id="ListParameter">
					<thead>
						<tr>
							<th class="bg-blue">Value Name</th>
							<th class="bg-blue">StartType</th>
							<th class="bg-blue">StartValue</th>
							<th class="bg-blue">EndType</th>
							<th class="bg-blue">EndValue</th>
							<th class="bg-blue">Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<select class="form-control">
					    			<option value="">In Range</option>
					    			<option value="01">Not In Range</option>
					    		</select>
    						</td>
    						<td>
    							<select class="form-control">
					    			<option value="">Delta</option>
					    			<option value="01">None</option>
					    			<option value="02">CurrTime</option>
					    		</select>
    						</td>
    						<td>
    						
    						</td>
							<td>
								<select class="form-control">
					    			<option value="">Delta</option>
					    			<option value="01">None</option>
					    			<option value="02">CurrTime</option>
					    		</select>
							</td>
							<td>
							
							</td>
							<td>
								Edit/Delete
							</td>
						
						</tr>
					</tbody>
				</table>
				</div>								
		</div>
    	

  <!-- Modal List Value load -->
  <div class="modal fade" id="ListValue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
        <div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">List Values</h4>
			</div>
			<div class="modal-body">
				<div class="form-group ">
					<input class="btn bg-gray" name="" id="" type="text" value="Add New Value" onclick="AddListValue();" />
					<input class="btn bg-gray" name="" id="" type="text" value="Apply" />
					<input class="btn bg-gray" data-dismiss="modal" type="text" value="Close" />
				</div>
				<div class="form-group ">
					<table class="table table-bordered" id="dataListValue">
					<thead>
						<tr>
							<th class="bg-blue">Value Index</th>
							<th class="bg-blue">Value Id</th>
							<th class="bg-blue">Value Name</th>
							<th class="bg-blue">Description</th>
							<th class="bg-blue">Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<input type="text" value="1" />
							</td>
							<td>
								<input type="text" value="1" />
							</td>
								
							<td>
								<input type="text" value="Name1" />
							</td>
								
							<td>
								<input type="text" value="Noi mang" />
							</td>
							<td>
								Edit/Delete/Copy/Paste
							</td>
						</tr>
					</tbody>
				</table>
				</div>
			</div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
  <!-- End Modal List Value load -->
  <script>
  		$('#_inputField_id').val("craMsg.eventTimeStamp");
		function AddChild(){
				var value_append = '<tr>'+
										'<td>'+
											'<select class="form-control">'+
											'	<option value="">Path/ChildPath</option>'+
											'	<option value="01">Path/ChildPath 01</option>'+
											'</select>'+
										'</td>'+
										'<td>'+
											'<select class="form-control">'+
												'<option value="">Filter</option>'+
												'<option value="01">Filter 01</option>'+
											'</select>'+
										'</td>'+
									'</tr>';
				//alert(value_append);
				
				$('#ListChild').append(value_append);
			  
		  }
		 function processCurringTime(){
			if ($('#curingtime').is(":checked"))
			{
				$('#_inputField_id').val("craMsg.eventTimeStamp");
				$('#_inputField_id').prop('disabled',true);
			  	$('#hideshow').hide();
			}else{
				$('#_inputField_id').val("craMsg.eventTimeStamp");
				$('#_inputField_id').prop('disabled',false);
				$('#hideshow').show();
			  	
			}
		}
		 function processStatic(){
				if ($('#static').is(":checked"))
				{
					$('#specialFields').val("isCurrTimeUsing:false;isStaticInput:true");
					
				  	
				}else{
					$('#specialFields').val("isCurrTimeUsing:false;isStaticInput:false");
					
				}
			}
		 
		 function AddListValue(){
				var value_append = '<tr>'+
										'<td>'+
											'<input type="text" value="" />'+
										'</td>'+
										'<td>'+
											'<input type="text" value="" />'+
										'</td>'+
										'<td>'+
											'<input type="text" value="" />'+
										'</td>'+
										'<td>'+
											'<input type="text" value="" />'+
										'</td>'+
										'<td>'+
										'</td>'+
									'</tr>';
				//alert(value_append);
				
				$('#dataListValue').append(value_append);
			  
		}
		function AddParameter(){
			var value_append = '<tr>'
									+'<td>'
									+'<select class="form-control">'
									+'<option value="">In Range</option>'
									+'<option value="01">Not In Range</option>'
									+'</select>'
									+'</td>'
									+'<td>'
									+'<select class="form-control">'
									+'<option value="">Delta</option>'
									+'<option value="01">None</option>'
									+'<option value="02">CurrTime</option>'
									+'</select>'
									+'</td>'
									+'<td>'
									
									+'</td>'
									+'<td>'
									+'<select class="form-control">'
									+'<option value="">Delta</option>'
									+'<option value="01">None</option>'
									+'<option value="02">CurrTime</option>'
									+'</select>'
									+'</td>'
									+'<td>'
									+'</td>'
									+'<td>'
									+'Edit/Delete'
									+'</td>'
								
									+'</tr>';


		$('#ListParameter').append(value_append);
		
		}
</script>

  
  
  
  
  
  
  
  
  
  
