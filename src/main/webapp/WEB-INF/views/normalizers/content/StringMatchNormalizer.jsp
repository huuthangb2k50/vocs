<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>


    <div id="content">
    	<div class="col-md-12">
    		<div class="form-group" id="_c_com_nw_domain_Normalizer_inputField_id">
    			<label class="col-sm-2 control-label" for="_inputField_id">Input Field 1</label>
    			<div class="col-sm-10"><input id="_inputField_id" name="inputField" class="form-control" value="" type="text"><br></div>
    		</div>
    	</div>
    	<div class="col-md-12">
    		
    		<div class="col-md-2">
    			<input type="button" class="form-control btn bg-gray" value="Add Child" onclick="AddChild1();" />
    		</div>
    		<div class="col-md-10">
				<table class="table table-bordered" id="ListChild1">
					<thead>
						<tr>
							<th class="bg-blue col-md-4">Path/ChildPath</th>
							<th class="bg-blue col-md-4">Filter</th>
							<th class="bg-blue col-md-4">Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<select class="form-control" data-toggle="modal" data-target="#ListPath">
					    			<option value="">Path/ChildPath</option>
					    			<option value="01">Path/ChildPath 01</option>
					    		</select>
    						</td>
							<td>
								<select class="form-control">
					    			<option value="">Filter</option>
					    			<option value="01">Filter 01</option>
					    		</select>
							</td>
							<td>
								Edit/Delete
							</td>
						</tr>
					</tbody>
				</table>
												
			</div>
    		
    	</div>
    	<div class="col-md-12">
    		<div class="form-group" id="_c_com_nw_domain_Normalizer_inputField_id">
    			<label class="col-sm-2 control-label" for="_inputField_id">Input Field 2</label>
    			<div class="col-sm-10"><input id="_inputField_id" name="inputField" class="form-control" value="" type="text"><br></div>
    		</div>
    	</div>
    	<div class="col-md-12">
    		
    		<div class="col-md-2">
    			<input type="button" class="form-control btn bg-gray" value="Add Child" onclick="AddChild2();" />
    		</div>
    		<div class="col-md-10">
				<table class="table table-bordered" id="ListChild2">
					<thead>
						<tr>
							<th class="bg-blue col-md-4">Path/ChildPath</th>
							<th class="bg-blue col-md-4">Filter</th>
							<th class="bg-blue col-md-4">Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<select class="form-control" data-toggle="modal" data-target="#ListPath">
					    			<option value="">Path/ChildPath</option>
					    			<option value="01">Path/ChildPath 01</option>
					    		</select>
    						</td>
							<td>
								<select class="form-control">
					    			<option value="">Filter</option>
					    			<option value="01">Filter 01</option>
					    		</select>
							</td>
							<td>
								Edit/Delete
							</td>
						</tr>
					</tbody>
				</table>
												
			</div>
    		
    	</div>
    	<div class="col-md-12">
    		<div class="form-group" id="c_com_nw_domain_Normalizer_specialFields">
    			<label class="col-sm-2 control-label" for="c_com_nw_domain_Normalizer_specialFields">Special Filed</label>
    			<div class="col-sm-10"><input id="c_com_nw_domain_Normalizer_specialFields" name="specialFields" class="form-control" value="" type="text"><br></div>
    		</div>
    	</div>
    	<div class="col-md-12">
	        <div class="form-group" id="">
	        	<label class="col-sm-2 control-label" for="">Start Charactor</label>
	        	<div class="col-md-4">
	        		<input id="" name="" class="form-control" value="" type="text">
		        	<br />
		        </div>
	        	<label class="col-sm-2 control-label" for="">End Charactor</label>
	        	<div class="col-md-4">
	        		<input id="" name="" class="form-control" value="" type="text">
		        	<br />
		        </div>
	        </div>
	    </div>
    	<div class="col-md-12">
    		
    		<div class="form-group" id="_c_com_nw_domain_Normalizer_defaultValue_id">
    			<label class="col-sm-2 control-label" for="_defaultValue_id">Default Value</label>
    			<div class="col-sm-4">
    			
	    			<select id="_defaultValue_id" name="defaultValue" class="form-control">
		                <option value="Equal">Equal</option>
                		<option selected="" value="Not Equal">Not Equal</option>
		            </select>
	    			<br>
    			</div>
    		</div>
    	</div>
    	
    	
 <!-- Data Modal -->
  
  
  <!-- Modal List Path load -->
  <div class="modal fade" id="ListPath" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
        <div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Select an Object Path</h4>
			</div>
			<div class="modal-body">
	                    <form action="#" method="get" class="sidebar-form">
	                        <div class="input-group">
	                          <input name="q" class="form-control" placeholder="Search..." type="text">
	                              <span class="input-group-btn">
	                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
	                                </button>
	                              </span>
	                        </div>
	                  </form>
	                    <div id="Treemenu1" class="tree clearfix" style="height: 50vh;">
	                        <ul class="dad">
	                            <li class="parent_li">
	                                <span title="Collapse this branch"><i class="fa fa-minus-square-o"></i></span> 
	                                	<a href="#"><i class="ti-shopping-cart-full"></i>
	                                 		<span id="context" data-toggle="context" data-target="#context-menu">
	                                 			Balance Range
	                                 		</span>
	                                 	</a>
	                                <ul>
	                                    <li>
	                                        <span><i class="fa fa-minus-square-o"></i></span> <a href="#"><i class="ti-gallery"></i>
	                                         <span id="context" data-toggle="context" data-target="#context-menu">
	                                         BR Data Over 100Mb
	                                         </span>
	                                         </a>
	                                    </li>
	                                </ul>
	                            </li>
	                            
	                        </ul>
	    
	                    </div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary" name="submit" >Select</button>
				<button type="button" class="btn btn-default " data-dismiss="modal">Cancel</button>
			</div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
  <!-- End Modal List Path load -->
  
  
  <script>
  function AddChild1(){
		var value_append = '<tr>'+
								'<td>'+
									'<select class="form-control">'+
									'	<option value="">Path/ChildPath</option>'+
									'	<option value="01">Path/ChildPath 01</option>'+
									'</select>'+
								'</td>'+
								'<td>'+
									'<select class="form-control">'+
										'<option value="">Filter</option>'+
										'<option value="01">Filter 01</option>'+
									'</select>'+
								'</td>'+
								'<td>'+
									'Edit/Delete '+
								'</td>'+
							'</tr>';
		//alert(value_append);
		
		$('#ListChild1').append(value_append);
	  
}
  function AddChild2(){
		var value_append = '<tr>'+
								'<td>'+
									'<select class="form-control" data-toggle="modal" data-target="#ListPath">'+
									'	<option value="">Path/ChildPath</option>'+
									'	<option value="01">Path/ChildPath 01</option>'+
									'</select>'+
								'</td>'+
								'<td>'+
									'<select class="form-control">'+
										'<option value="">Filter</option>'+
										'<option value="01">Filter 01</option>'+
									'</select>'+
								'</td>'+
								'<td>'+
									'Edit/Delete '+
								'</td>'+
							'</tr>';
		//alert(value_append);
		
		$('#ListChild2').append(value_append);
	  
}

  </script>