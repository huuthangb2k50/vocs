<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

    	<div class="col-md-12">
    		<div class="form-group" id="_c_com_nw_domain_Normalizer_inputField_id">
    			<label class="col-sm-2 control-label" for="_inputField_id">Input Field</label>
    			<div class="col-sm-10"><input id="_inputField_id" name="inputField" class="form-control" value="" type="text"><br></div>
    		</div>
    	</div>
    	<div class="col-md-12">
    		<div class="col-md-2">
    			<input type="button" class="form-control btn bg-gray" value="Add Child" onclick="AddChild1();" />
    		</div>
    		<div class="col-md-10">
				<table class="table table-bordered" id="ListChild1">
					<thead>
						<tr>
							<th class="bg-blue">Path/ChildPath</th>
							<th class="bg-blue">Filter</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<select class="form-control" data-toggle="modal" data-target="#ListPath">
					    			<option value="">Path/ChildPath</option>
					    			<option value="01">Path/ChildPath 01</option>
					    		</select>
    						</td>
							<td>
								<select class="form-control">
					    			<option value="">Filter</option>
					    			<option value="01">Filter 01</option>
					    		</select>
							</td>
						</tr>
					</tbody>
				</table>
												
			</div>
    		
    	</div>
    	
    	<div class="col-md-12">
    		<div class="form-group" id="_c_com_nw_domain_Normalizer_inputField_id">
    			<label class="col-sm-2 control-label" for="_inputField_id">Input Field</label>
    			<div class="col-sm-10"><input id="_inputField_id" name="inputField" class="form-control" value="" type="text"><br></div>
    		</div>
    	</div>
    	<div class="col-md-12">
    		<div class="col-md-2">
    			<input type="button" class="form-control btn bg-gray" value="Add Child" onclick="AddChild2();" />
    		</div>
    		<div class="col-md-10">
				<table class="table table-bordered" id="ListChild2">
					<thead>
						<tr>
							<th class="bg-blue">Path/ChildPath</th>
							<th class="bg-blue">Filter</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<select class="form-control" data-toggle="modal" data-target="#ListPath">
					    			<option value="">Path/ChildPath</option>
					    			<option value="01">Path/ChildPath 01</option>
					    		</select>
    						</td>
							<td>
								<select class="form-control">
					    			<option value="">Filter</option>
					    			<option value="01">Filter 01</option>
					    		</select>
							</td>
						</tr>
					</tbody>
				</table>
												
			</div>
    		
    	</div>
    	<div class="col-md-12">
    		
    		<div class="form-group" id="_c_com_nw_domain_Normalizer_defaultValue_id">
    			<label class="col-sm-2 control-label" for="_defaultValue_id">Default Value</label>
    			<div class="col-sm-4">
    			
	    			<select id="_defaultValue_id" name="defaultValue" class="form-control">
		                <option value="NotExist">Not Exist</option>
		                <option value="Exist">Exist</option>
		            </select>
	    			<br>
    			</div>
    		</div>
    	</div>
    
    
    	
  <script>
  function AddChild1(){
		var value_append = '<tr>'+
								'<td>'+
									'<select class="form-control">'+
									'	<option value="">Path/ChildPath</option>'+
									'	<option value="01">Path/ChildPath 01</option>'+
									'</select>'+
								'</td>'+
								'<td>'+
									'<select class="form-control">'+
										'<option value="">Filter</option>'+
										'<option value="01">Filter 01</option>'+
									'</select>'+
								'</td>'+
							'</tr>';
		//alert(value_append);
		
		$('#ListChild1').append(value_append);
	  
  }
  function AddChild2(){
		var value_append = '<tr>'+
								'<td>'+
									'<select class="form-control">'+
									'	<option value="">Path/ChildPath</option>'+
									'	<option value="01">Path/ChildPath 01</option>'+
									'</select>'+
								'</td>'+
								'<td>'+
									'<select class="form-control">'+
										'<option value="">Filter</option>'+
										'<option value="01">Filter 01</option>'+
									'</select>'+
								'</td>'+
							'</tr>';
		//alert(value_append);
		
		$('#ListChild2').append(value_append);
	  
}
  
  
  </script>
  
  
