<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

  <div id="content">
    	<div class="col-md-12">
    		<div class="form-group" id="_c_com_nw_domain_Normalizer_inputField_id">
    			<label class="col-sm-2 control-label" for="_inputField_id">Input Field</label>
    			<div class="col-sm-10"><input id="_inputField_id" name="inputField" class="form-control" value="" type="text"><br></div>
    		</div>
    	</div>
    	<div class="col-md-12">
    		
    		<div class="col-md-2">
    			<input type="button" class="form-control btn bg-gray" value="Add Child" onclick="AddChild();" />
    		</div>
    		<div class="col-md-10">
				<table class="table table-bordered" id="ListChild">
					<thead>
						<tr>
							<th class="bg-blue col-md-4">Path/ChildPath</th>
							<th class="bg-blue col-md-4">Filter</th>
							<th class="bg-blue col-md-4">Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<select class="form-control" data-toggle="modal" data-target="#ListPath">
					    			<option value="">Path/ChildPath</option>
					    			<option value="01">Path/ChildPath 01</option>
					    		</select>
    						</td>
							<td>
								<select class="form-control">
					    			<option value="">Filter</option>
					    			<option value="01">Filter 01</option>
					    		</select>
							</td>
							<td>
								Edit/Delete
							</td>
						</tr>
					</tbody>
				</table>
												
			</div>
    		
    	</div>
    	
    	<div class="col-md-12">
    		
    		<div class="form-group" id="_c_com_nw_domain_Normalizer_defaultValue_id">
    			<label class="col-sm-2 control-label" for="_defaultValue_id">Default Value</label>
    			<div class="col-sm-4">
    			
	    			<select id="_defaultValue_id" name="defaultValue" class="form-control">
		                <option value="None">None</option>
		                <option value="Hai Phong">Hai Phong</option>
		                <option value="Ha noi">Ha noi</option>
		                <option value="Promotion">Promotion</option>
		            </select>
	    			<br>
    			</div>
    		</div>
    	</div>
    	
    	<div class="col-md-12">
    		<div class="col-md-2">
    			<input type="button" class="form-control btn bg-gray " value="Edit Values" data-toggle="modal" data-target="#ListValue" />
    		</div>
    		<div class="col-md-2">
    			<input type="button" class="form-control btn bg-gray " value="Add New Parameter" onclick="AddParameter();" />
    		</div>
    		<div class="col-md-2">
    			<input type="button" class="form-control btn bg-gray " value="Validate" onclick="" /><br />
    		</div>

    	</div>
    	
    	<div class="col-md-12 " style="padding-top: 20px;">
    		<div class="col-md-12 ">
				<table class="table table-bordered" id="ListParameter">
					<thead>
						<tr>
							<th class="bg-blue col-xd-3">Value Name</th>
							<th class="bg-blue col-xd-3">Zone/ZoneMap</th>
							<th class="bg-blue col-xd-3">ZoneId/ZoneMapId</th>
							<th class="bg-blue col-xd-3">Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<select class="form-control">
					    			<option value="None">None</option>
						                <option value="Hai Phong">Hai Phong</option>
						                <option value="Ha noi">Ha noi</option>
						                <option value="Promotion">Promotion</option>
					    		</select>
    						</td>
    						
							
							
						</tr>
					</tbody>
				</table>
			</div>								
		</div>
	</div>
  <div class="modal fade" id="ListPath" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
        <div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Select an Object Path</h4>
			</div>
			<div class="modal-body">
	                    <form action="#" method="get" class="sidebar-form">
	                        <div class="input-group">
	                          <input name="q" class="form-control" placeholder="Search..." type="text">
	                              <span class="input-group-btn">
	                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
	                                </button>
	                              </span>
	                        </div>
	                  </form>
	                    <div id="Treemenu1" class="tree clearfix" style="height: 50vh;">
	                        <ul class="dad">
	                            <li class="parent_li">
	                                <span title="Collapse this branch"><i class="fa fa-minus-square-o"></i></span> 
	                                	<a href="#"><i class="ti-shopping-cart-full"></i>
	                                 		<span id="context" data-toggle="context" data-target="#context-menu">
	                                 			Balance Range
	                                 		</span>
	                                 	</a>
	                                <ul>
	                                    <li>
	                                        <span><i class="fa fa-minus-square-o"></i></span> <a href="#"><i class="ti-gallery"></i>
	                                         <span id="context" data-toggle="context" data-target="#context-menu">
	                                         BR Data Over 100Mb
	                                         </span>
	                                         </a>
	                                    </li>
	                                </ul>
	                            </li>
	                            
	                        </ul>
	    
	                    </div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary" name="submit" >Select</button>
				<button type="button" class="btn btn-default " data-dismiss="modal">Cancel</button>
			</div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
  <div class="modal fade" id="ListValue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
        <div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">List Values</h4>
			</div>
			<div class="modal-body">
				<div class="form-group ">
					<input class="btn bg-gray" name="" id="" type="text" value="Add New Value" onclick="AddListValue();" />
					<input class="btn bg-gray" name="" id="" type="text" value="Apply" />
					<input class="btn bg-gray" data-dismiss="modal" type="text" value="Close" />
				</div>
				<div class="form-group ">
					<table class="table table-bordered" id="dataListValue">
					<thead>
						<tr>
							<th class="bg-blue">Value Index</th>
							<th class="bg-blue">Value Id</th>
							<th class="bg-blue">Value Name</th>
							<th class="bg-blue">Description</th>
							<th class="bg-blue">Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<input type="text" value="1" />
							</td>
							<td>
								<input type="text" value="1" />
							</td>
								
							<td>
								<input type="text" value="Name1" />
							</td>
								
							<td>
								<input type="text" value="Noi mang" />
							</td>
							<td>
								Edit/Delete/Copy/Paste
							</td>
						</tr>
					</tbody>
				</table>
				</div>
			</div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
  <!-- End Modal List Value load -->
  
  
  
  
  <script>
  function AddChild(){
		var value_append = '<tr>'+
								'<td>'+
									'<select class="form-control">'+
									'	<option value="">Path/ChildPath</option>'+
									'	<option value="01">Path/ChildPath 01</option>'+
									'</select>'+
								'</td>'+
								'<td>'+
									'<select class="form-control">'+
										'<option value="">Filter</option>'+
										'<option value="01">Filter 01</option>'+
									'</select>'+
								'</td>'+
							'</tr>';
		//alert(value_append);
		
		$('#ListChild').append(value_append);
	  
  }
  function AddListValue(){
		var value_append = '<tr>'+
								'<td>'+
									'<input type="text" value="" />'+
								'</td>'+
								'<td>'+
									'<input type="text" value="" />'+
								'</td>'+
								'<td>'+
									'<input type="text" value="" />'+
								'</td>'+
								'<td>'+
									'<input type="text" value="" />'+
								'</td>'+
								'<td>'+
								'</td>'+
							'</tr>';
		//alert(value_append);
		
		$('#dataListValue').append(value_append);
	  
}
  function AddParameter(){
		var value_append = '<tr>'+
								'<td>'+
									'<select class="form-control">'+
						    			'<option value="">Noi mang</option>'+
						    			'<option value="01">Ngoai mang</option>'+
						    		'</select>'+
	    						'</td>'+
	    						'<td>'+
	    							'<input type="text" value="" class="form-control" />'+
	    						'</td>'+
								'<td>'+
									'<select class="form-control">'+
						    			'<option value="">Prefix</option>'+
						    			'<option value="01">Suffix</option>'+
						    		'</select>'+
								'</td>'+
								'<td>'+
									'Delete/Edit/Copy/Cut/Paste'+
								'</td>'+
							'</tr>';
		//alert(value_append);
		
		$('#ListParameter').append(value_append);
	  
}
      
  
  
  
  
  
  
  
  
  
  