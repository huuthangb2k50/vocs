<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Show Zones</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
    <page:show id="ps_com_nw_domain_Normalizer" object="${normalizer}" path="/normalizers" z="f9DxHwaLLT4IY5//W6KFCIq/IFw=">
        <field:display field="normalizerName" id="s_com_nw_domain_Normalizer_normalizerName" object="${normalizer}" z="aP/nDa1szBynnS3eoC4DPjSD+44="/>
        <field:display field="normalizerType" id="s_com_nw_domain_Normalizer_normalizerType" object="${normalizer}" z="tJQXr/fijCk4ZhK4Zt95h89q+bI="/>
        <field:display field="normalizerState" id="s_com_nw_domain_Normalizer_normalizerState" object="${normalizer}" z="T4TY78N9D5gKCXcsJO14pHEFH6o="/>
        <field:display field="defaultValue" id="s_com_nw_domain_Normalizer_defaultValue" object="${normalizer}" z="y73K/F5lNFXl8zB1JpyceXCGKI0="/>
        <field:display field="valueIfNull" id="s_com_nw_domain_Normalizer_valueIfNull" object="${normalizer}" z="9lYqVb3i8T0QWNCOmR1oWovbFls="/>
        <field:display field="inputField" id="s_com_nw_domain_Normalizer_inputField" object="${normalizer}" z="Wp5X11LhruIp9TliFYD2OAyMhHk="/>
        <field:display field="specialFields" id="s_com_nw_domain_Normalizer_specialFields" object="${normalizer}" z="XV2V6pGSGgUKUirQznOSPpqDyLQ="/>
        <field:display field="categoryId" id="s_com_nw_domain_Normalizer_categoryId" object="${normalizer}" z="3X4IywNEpv5hUkp61WVEChL0SfU="/>
        <field:display field="queryState" id="s_com_nw_domain_Normalizer_queryState" object="${normalizer}" z="mgLTGAMVgPlhOKhxMwWOtg7CuB4="/>
    </page:show>
    
    </div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>   