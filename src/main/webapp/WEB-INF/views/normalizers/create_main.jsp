<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<c:set var="myContext" value="${pageContext.request.contextPath}"></c:set>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Normalizers<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Balance Range
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         BR Data Over 100Mb
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Create Normalizer</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">

    <form:create id="fc_com_nw_domain_Normalizer" modelAttribute="normalizer" path="/normalizers" render="${empty dependencies}" z="kxR/3TKW1qo/su1WOW66zkmxrCo=">
        <div class="col-md-6">

        <field:input field="normalizerName" id="c_com_nw_domain_Normalizer_normalizerName" z="PbQR3bj4ligpJu3tUOsRvsdga9k="/>

        <field:input field="Description" id="c_com_nw_domain_Normalizer_Description" z="CEmyXsXPTZ9kKaUe8Kx+0mxsBuE="/>
        <field:datetime dateTimePattern="${normalizer_effdate_date_format}" field="EffDate" id="c_com_nw_domain_Normalizer_EffDate" z="BWohY1epWwLkwTE0nu8DdNrThFU="/>
        <field:datetime dateTimePattern="${normalizer_expdate_date_format}" field="ExpDate" id="c_com_nw_domain_Normalizer_ExpDate" z="TXLoqWVCgTIjPfvjRj+uAQVIFJ8="/>
        
        
        </div>
        <div class="col-md-6">
	        <field:input field="normalizerType" id="c_com_nw_domain_Normalizer_normalizerType" validationMessageCode="field_invalid_integer" z="ymyApRcowfYRCnmpeFAdmJgtyzQ="/>
	        <field:input field="normalizerState" id="c_com_nw_domain_Normalizer_normalizerState" validationMessageCode="field_invalid_integer" z="2mowRUnxuajXChio/BvQjIN5V7E="/>
	        <field:select field="categoryId" id="c_com_nw_domain_Normalizer_categoryId" itemValue="categoryId" items="${categorys}" path="/categorys" z="wc6/yBuSs0EZM5jFYnotRIdKIQw="/>
	        	        
	        <%-- <field:input field="valueIfNull" id="c_com_nw_domain_Normalizer_valueIfNull" validationMessageCode="field_invalid_integer" z="Yp1qiwhqpB4vVrLyJsIcM80jPA8="/>
	        
	        <field:input field="specialFields" id="c_com_nw_domain_Normalizer_specialFields" z="usVZx3VS0TsjBO+5mXvQ+ccVDX0="/>
	        	        
	        <field:input field="queryState" id="c_com_nw_domain_Normalizer_queryState" validationMessageCode="field_invalid_integer" z="GHu2AngowpUfxYpJDQ8gdZSyXpI="/> --%>
    	</div>
    	<div class="col-md-12">
    		<field:input field="inputField" id="c_com_nw_domain_Normalizer_inputField" z="ahwo3H9Bo1dmTBVjTIVp6v05Tgg="/>
    		
    		<div class="col-md-3">
    			<input type="button" class="form-control btn bg-gray" value="Add Child" onclick="AddChild();" />
    		</div>
    		<div class="col-md-9">
				<table class="table table-bordered" id="ListChild">
					<thead>
						<tr>
							<th class="bg-blue">Path/ChildPath</th>
							<th class="bg-blue">Filter</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<select class="form-control">
					    			<option value="">Path/ChildPath</option>
					    			<option value="01">Path/ChildPath 01</option>
					    		</select>
    						</td>
							<td>
								<select class="form-control">
					    			<option value="">Filter</option>
					    			<option value="01">Filter 01</option>
					    		</select>
							</td>
						</tr>
					</tbody>
				</table>
												
			</div>
    		
    	</div>
    	<div class="col-md-12">
    		<field:input field="defaultValue" id="c_com_nw_domain_Normalizer_defaultValue" validationMessageCode="field_invalid_integer" z="k707CNGbu/TplQa+LfnXLAC7k1A="/>
    	</div>
    	
    	<div class="col-md-12">
    		<input type="button" class="btn bg-gray margin" value="Edit Values" onclick="" />

    		<input type="button" class="btn bg-gray margin" value="Add New Parameter" onclick="" />

    		<input type="button" class="btn bg-gray margin" value="Validate" onclick="" />
    	</div>
    	<div class="col-md-12 box-body">
				<table class="table table-bordered" id="">
					<thead>
						<tr>
							<th class="bg-blue">Value Name</th>
							<th class="bg-blue">Parameter Value</th>
							<th class="bg-blue">Compare Type</th>
							<th class="bg-blue">Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<select class="form-control">
					    			<option value="">Noi mang</option>
					    			<option value="01">Ngoai mang</option>
					    		</select>
    						</td>
    						<td>
    						
    						</td>
							<td>
								<select class="form-control">
					    			<option value="">Prefix</option>
					    			<option value="01">Suffix</option>
					    		</select>
							</td>
							<td>
							
							</td>
						</tr>
					</tbody>
				</table>
												
		</div>
    	<div class="col-md-12">
    		<input type="button" class="btn bg-gray margin" value="Edit" onclick="" />

    		<input type="button" class="btn bg-gray margin" value="Apply" onclick="getcontent();" />

    		<input type="button" class="btn bg-gray margin" value="Cancel" onclick="" />
    	</div>
    	

    </form:create>
    <form:dependency dependencies="${dependencies}" id="d_com_nw_domain_Normalizer" render="${not empty dependencies}" z="OtKuw9i0exdzR6kW0Q/LJJqJnxM="/>
	    	
                        </div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>
  
<script type="text/javascript">
	
	function getcontent(){
		//get value type normalizer
		var type = 2 ;
		alert(type);
		$.ajax({
	        url : '${myContext}/normalizers/time/ajax',
	        data: 'type='+type ,
	        type: 'POST',
	        contentType: "application/x-www-form-urlencoded;charset=utf-8",
	        success : function(data) {
	        	alert(data);
	        	window.location.reload();
	        }
	    });
	}


</script>
  
  
  
  
  
  
  
  
  
  
