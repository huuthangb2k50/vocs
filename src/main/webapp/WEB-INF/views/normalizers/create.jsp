<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<c:set var="myContext" value="${pageContext.request.contextPath}"></c:set>

<c:set var="myContext" value="${pageContext.request.contextPath}"></c:set>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Normalizers<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Balance Range
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         BR Data Over 100Mb
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Create Normalizer</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Create Normalizer</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">

    <form:create id="fc_com_nw_domain_Normalizer" modelAttribute="normalizer" path="/normalizers" render="${empty dependencies}" z="kxR/3TKW1qo/su1WOW66zkmxrCo=">
      
        <div class="col-md-6">

        <field:input field="normalizerName" id="c_com_nw_domain_Normalizer_normalizerName" z="PbQR3bj4ligpJu3tUOsRvsdga9k="/>

        <field:input field="Description" id="c_com_nw_domain_Normalizer_Description" z="CEmyXsXPTZ9kKaUe8Kx+0mxsBuE="/>
        <field:datetime dateTimePattern="${normalizer_effdate_date_format}" field="EffDate" id="c_com_nw_domain_Normalizer_EffDate" z="BWohY1epWwLkwTE0nu8DdNrThFU="/>
        <field:datetime dateTimePattern="${normalizer_expdate_date_format}" field="ExpDate" id="c_com_nw_domain_Normalizer_ExpDate" z="TXLoqWVCgTIjPfvjRj+uAQVIFJ8="/>
        
        
        </div>
        <div class="col-md-6">
	        
	        <div class="form-group" id="_c_com_nw_domain_Normalizer_normalizerType_id">
	        	<label class="col-sm-3 control-label" for="_normalizerType_id">Normalizer Type</label>
	        	<div class="col-md-9">
		        	<select class="form-control" id="_normalizerType_id" name="normalizerType" onchange="getContentNormalizer(this.options[ this.selectedIndex ].value);">
			          <option value="StringNormalizer">String Normalizer</option>
			          <option value="StringMatchNormalizer">String Match Normalizer</option>
			          <option value="NumberNormalizer">Number Normalizer</option>
			          <option value="CheckRegisterListNormalizer">Check Register List Normalizer</option>
			          <option value="TimeNormalizer">Time Normalizer</option>
			          <option value="DateNormalizer">Date Normalizer</option>
			          <option value="QuantityNormalizer">Quantity Normalizer</option>
			          <option value="BalanceNormalizer">Balance Normalizer</option>
			          <option value="ZoneNormalizer">Zone Normalizer</option>
			          <option value="CheckInListNormalizer">Check In List Normalizer</option>
			          <option value="AcmBalanceNormalizer">AcmBalance Normalizer</option>
		        	</select>
		        	<br />
		        </div>
	        </div>
	        <br />
	        
	        <div class="form-group" id="_c_com_nw_domain_Normalizer_normalizerState_id">
	        	<label class="col-sm-3 control-label" for="_normalizerState_id">Normalizer State</label>
	        	<div class="col-md-9">
		        	<select class="form-control" id="_normalizerState_id" name="normalizerState">
			          	<option value="Dynamic">Dynamic</option>
          				<option value="Static">Static</option>
		        	</select>
		        	<br />
		        </div>
	        </div>
	        <br />
	        
	        
	        <field:select field="categoryId" id="c_com_nw_domain_Normalizer_categoryId" itemValue="categoryId" items="${categorys}" path="/categorys" z="wc6/yBuSs0EZM5jFYnotRIdKIQw="/>
	        	        

	    </div>
    	
    	
    	<div id="content">
    		<%-- <%@include file="content/StringNormalizer.jsp" %> --%>

		</div>

		
		
    	<div class="col-md-12">
    		<div class="col-md-2">
    			<input type="button" class="form-control btn bg-gray " value="Edit" onclick="" />
    		</div>
    		<div class="col-md-2">
    			<input type="button" class="form-control btn bg-gray " value="Apply" onclick="" />
    		</div>
    		<div class="col-md-2">
    			<input type="button" class="form-control btn bg-gray " value="Cancel" onclick="" />
    		</div>
    	</div>
    	

    </form:create>
    <form:dependency dependencies="${dependencies}" id="d_com_nw_domain_Normalizer" render="${not empty dependencies}" z="OtKuw9i0exdzR6kW0Q/LJJqJnxM="/>
	    	
                        </div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>
  


  

  <script>

  function getContentNormalizer(type){
	  //alert(type);
	  $.ajax(
			  {
	        url : '${myContext}/normalizers/testControllerURL',
	        data: 'type='+type ,
	        type: 'GET',
	        contentType: "application/x-www-form-urlencoded;charset=utf-8",
	        success : function(data) {
	        	//alert(data);
	            $('#content').html(data);
	        }
	  });
	  
  }

  
  
  
  </script>
  
  
