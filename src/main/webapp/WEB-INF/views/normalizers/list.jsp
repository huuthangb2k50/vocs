<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3">
                <div class="Submenu">
                    <h3>Rate Tables</h3>
					<form action="#" method="get" class="sidebar-form">
					    <div class="input-group">
					      <input name="q" class="form-control" placeholder="Search..." type="text">
					          <span class="input-group-btn">
					            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
					            </button>
					          </span>
					    </div>
					</form>
                    <div id="ocsTree" class="tree clearfix"></div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Rate Table for Purchase</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
                <section class="content">
                	<div class="well well-sm fillter">
                    	<form class="form-inline">
                            <div class="form-group">
                                <input class="form-control" id="exampleInputEmail3" placeholder="Threshold name" type="text">
                            </div>
                            <div class="form-group">
                                <select class="form-control">
                                        <option value="0">Threshold Type</option>
                                        <option value="1">Online</option>
                                        <option value="2">Đang xử lý</option>
                                        <option value="3">Bị lỗi</option>
                                    </select>
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Fillter</button>
                            <a class="btn btn-default pull-right" data-toggle="modal" href="#Addnew10"><i class="fa fa-plus"></i> Add new</a>
                        </form>
                    </div>
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
						    <page:list id="pl_com_nw_domain_Normalizer" items="${normalizers}" z="avI6r106F1gUNuXJeCDtYA1/G28=">
						        <table:table data="${normalizers}" id="l_com_nw_domain_Normalizer" path="/normalizers" typeIdFieldName="normalizerId" z="j8viHAYIAXQMwJUEFm3ZMqxwcbU=">
						            <table:column id="c_com_nw_domain_Normalizer_normalizerName" property="normalizerName" z="/4EJmyNXkwtmdX5BwUuiW2gvYwY="/>
						            <table:column id="c_com_nw_domain_Normalizer_normalizerType" property="normalizerType" z="bk9t6Nyz7tO+KgilnXOd23qrcl4="/>
						            <table:column id="c_com_nw_domain_Normalizer_normalizerState" property="normalizerState" z="2Uaso6+xOBY1R+nxKDBtDkSd3RM="/>
						            <table:column id="c_com_nw_domain_Normalizer_defaultValue" property="defaultValue" z="//KP0TbdsYP2+BNqIzqMmNZ848U="/>
						            <table:column id="c_com_nw_domain_Normalizer_valueIfNull" property="valueIfNull" z="RkgJfRBsvPMNEOKV090aifbz0dI="/>
						            <table:column id="c_com_nw_domain_Normalizer_inputField" property="inputField" z="tliSgmidBU2XUN663qLQ3YL9LfA="/>
						        </table:table>
						    </page:list>
                        </div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
        </div>
    </section>
    <!-- /.content -->
</div>   

<script type="text/javascript">
	$(document).ready(function() {
		$('#ocsTree').ocsTree(TreeType.NORMALIZERS);
	});
</script>