<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Update Zones</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
    <form:update id="fu_com_nw_domain_Normalizer" idField="normalizerId" modelAttribute="normalizer" path="/normalizers" versionField="Version" z="WYTI8iwb0WfqBF4i0s6gOPdNabA=">
        <field:input field="normalizerName" id="c_com_nw_domain_Normalizer_normalizerName" z="PbQR3bj4ligpJu3tUOsRvsdga9k="/>
        <field:input field="normalizerType" id="c_com_nw_domain_Normalizer_normalizerType" validationMessageCode="field_invalid_integer" z="ymyApRcowfYRCnmpeFAdmJgtyzQ="/>
        <field:input field="normalizerState" id="c_com_nw_domain_Normalizer_normalizerState" validationMessageCode="field_invalid_integer" z="2mowRUnxuajXChio/BvQjIN5V7E="/>
        <field:input field="defaultValue" id="c_com_nw_domain_Normalizer_defaultValue" validationMessageCode="field_invalid_integer" z="k707CNGbu/TplQa+LfnXLAC7k1A="/>
        <field:input field="valueIfNull" id="c_com_nw_domain_Normalizer_valueIfNull" validationMessageCode="field_invalid_integer" z="Yp1qiwhqpB4vVrLyJsIcM80jPA8="/>
        <field:input field="inputField" id="c_com_nw_domain_Normalizer_inputField" z="ahwo3H9Bo1dmTBVjTIVp6v05Tgg="/>
        <field:input field="specialFields" id="c_com_nw_domain_Normalizer_specialFields" z="usVZx3VS0TsjBO+5mXvQ+ccVDX0="/>
        <field:select field="categoryId" id="c_com_nw_domain_Normalizer_categoryId" itemValue="categoryId" items="${categorys}" path="/categorys" z="wc6/yBuSs0EZM5jFYnotRIdKIQw="/>
        <field:input field="queryState" id="c_com_nw_domain_Normalizer_queryState" validationMessageCode="field_invalid_integer" z="GHu2AngowpUfxYpJDQ8gdZSyXpI="/>
    </form:update>
    
    					</div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>   