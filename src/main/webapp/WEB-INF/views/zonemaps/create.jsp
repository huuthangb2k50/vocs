<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Create zoneMap</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">zoneMap</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
	    <form:create id="fc_com_nw_domain_ZoneMap" modelAttribute="zoneMap" path="/zonemaps" render="${empty dependencies}" z="6bqmibiviA1npJN1dtrQzX7atYo=">
	        <field:input field="zoneMapName" id="c_com_nw_domain_ZoneMap_zoneMapName" z="fyEk71MMRPn9XB47Pw15l9GRGms="/>
	        <field:input field="remark" id="c_com_nw_domain_ZoneMap_remark" z="GPjruUBjNcP0CysHz8OusHz/jaI="/>
	    </form:create>
	    <form:dependency dependencies="${dependencies}" id="d_com_nw_domain_ZoneMap" render="${not empty dependencies}" z="XaIOmfK94jOIY7hdWvZdjhdOLBs="/>
	   
	    <page:list id="pl_com_nw_domain_GeoHomeZone" items="${result}" z="zVBX9VX7Yrgzf+Nl1pNETl5NS5M=">
        <table:table data="${result}" id="l_com_nw_domain_GeoHomeZone" path="/zonemaps" typeIdFieldName="geoHomeZoneId" z="ktOs6t0ScsotUBCHC59rkFUPTWE=">
            <table:column id="c_com_nw_domain_GeoHomeZone_geoHomeZoneName" property="geoHomeZoneName" z="xJKhTUukrLz5Hbxl1qmTUCJYgV4="/>
            <table:column id="c_com_nw_domain_GeoHomeZone_geoHomeZoneCode" property="geoHomeZoneCode" z="RcpcnaAbg/gRbWn7f3QXFpTy+qg="/>
            <table:column id="c_com_nw_domain_GeoHomeZone_remark" property="remark" z="83/x97WssQ7R2FPubwLjKWgI588="/>
            <table:column id="c_com_nw_domain_GeoHomeZone_geoHomeZoneType" property="geoHomeZoneType" z="iH5od44mU8g06BEVAnopI6C4CdY="/>
        </table:table>
    </page:list>
</div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>   