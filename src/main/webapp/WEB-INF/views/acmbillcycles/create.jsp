<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
    <section class="content">
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input name="q" class="form-control" placeholder="Search..." type="text">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                    <span  id="context" data-toggle="context" data-target="#context-menu">
                                        Offer Templates
                                    </span>
                                </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                            <span  id="context" data-toggle="context" data-target="#context-menu">
                                                MI Templates
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>

                    </div>
                </div>
            </div>
            <div class="col-xs-9">
                <div class="row">
                    <section class="content-header">
                        <h1>Create Acmbillcycles</h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </section>
                    <section class="content">
                        <div class="box">
                            <div class="box-body table-responsive">
                                <form:create id="fc_com_nw_domain_AcmBillCycle" modelAttribute="acmBillCycle" path="/acmbillcycles" render="${empty dependencies}" z="uZS28Pl0XVGVrKesErL5O0K0LZ0=">
                                    <field:input field="acmBillCycleId" id="c_com_nw_domain_AcmBillCycle_acmBillCycleId" validationMessageCode="field_invalid_integer" z="t3TZNAnwLEWclBfXYkHZE3/rWmU="/>
                                    <field:select field="balTypeId" id="c_com_nw_domain_AcmBillCycle_balTypeId" itemValue="balTypeId" items="${baltypes}" path="/baltypes" z="ZperOsuMKTkgyFsEsNmUMyEBQvc="/>
                                    <field:select field="billingCycleTypeId" id="c_com_nw_domain_AcmBillCycle_billingCycleTypeId" itemValue="billingCycleTypeId" items="${billingcycletypes}" path="/billingcycletypes" z="JsDaM2+30c1ZJO1CJ+ik7U2BszQ="/>
                                </form:create>
                                <form:dependency dependencies="${dependencies}" id="d_com_nw_domain_AcmBillCycle" render="${not empty dependencies}" z="uui68X2c1QvIBobFcapZcVFfV0Y="/>
                            </div>
                        </div>

                    </section>
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->
</div>   