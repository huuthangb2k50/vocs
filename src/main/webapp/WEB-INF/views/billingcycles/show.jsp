<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
    <section class="content">
        <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input name="q" class="form-control" placeholder="Search..." type="text">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                    <span  id="context" data-toggle="context" data-target="#context-menu">
                                        Offer Templates
                                    </span>
                                </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                            <span  id="context" data-toggle="context" data-target="#context-menu">
                                                MI Templates
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>

                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                    <!-- Content Header (Page header) -->
                    <!-- InstanceBeginEditable name="EditRegion1" -->
                    <section class="content-header">
                        <h1>Show BillingCycles</h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </section>
                    <section class="content">
                        <div class="box">

                            <!-- /.box-header -->
                            <div class="box-body table-responsive">
                                <page:show id="ps_com_nw_domain_BillingCycle" object="${billingcycle}" path="/billingcycles" z="IsxGmq1wf3+IzWjH5uozlWs3qF8=">
                                    <div class="col-lg-6">
                                    <field:display field="billingCycleTypeId" id="s_com_nw_domain_BillingCycle_billingCycleTypeId" object="${billingcycle}" z="z+jNDkkoXB0HrterqkUAXg8tHDU="/>
                                    <field:display date="true" dateTimePattern="${billingCycle_cyclebegindate_date_format}" field="cycleBeginDate" id="s_com_nw_domain_BillingCycle_cycleBeginDate" object="${billingcycle}" z="L8VfbGCmNnD/0Dp2gvOE2crhe0o="/>
                                    </div>
                                    <div class="col-lg-6">
                                    <field:display date="true" dateTimePattern="${billingCycle_cycleenddate_date_format}" field="cycleEndDate" id="s_com_nw_domain_BillingCycle_cycleEndDate" object="${billingcycle}" z="qJ6p9xQWPYqgRGf5iXBccWBtffc="/>
                                    <field:display field="states" id="s_com_nw_domain_BillingCycle_states" object="${billingcycle}" z="eIWkMUgcxSJhc/V5CUtnmVn0rwg="/>
                                    </div>
                                    </page:show>

                            </div>
                        </div>

                    </section>
                </div>
            </div>

        </div>
    </section>
</div>   