<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
    <section class="content">
        <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input name="q" class="form-control" placeholder="Search..." type="text">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                    <span  id="context" data-toggle="context" data-target="#context-menu">
                                        Offer Templates
                                    </span>
                                </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                            <span  id="context" data-toggle="context" data-target="#context-menu">
                                                MI Templates
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>

                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                    <!-- Content Header (Page header) -->
                    <!-- InstanceBeginEditable name="EditRegion1" -->
                    <section class="content-header">
                        <h1>Show Columndts</h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </section>
                    <section class="content">
                        <div class="box">

                            <!-- /.box-header -->
                            <div class="box-body table-responsive">
                                <page:show id="ps_com_nw_domain_ColumnDT" object="${columndt}" path="/columndts" z="zgLWIXNHn19EnpaRpw+X7jCWh9c=">
                                    <div class="col-lg-6">
                                    <field:display field="attribute" id="s_com_nw_domain_ColumnDT_attribute" object="${columndt}" z="Thk6rlEnCzZQ+gHIuwXAlvZBlM4="/>
                                    <field:display field="columnIndex" id="s_com_nw_domain_ColumnDT_columnIndex" object="${columndt}" z="AO+pVe6UOBxktyjR8OXjMyj2eJs="/>
                                    </div>
                                    <div class="col-lg-6">
                                    <field:display field="columnName" id="s_com_nw_domain_ColumnDT_columnName" object="${columndt}" z="gwNJpy60MQCu5Mppg3KwMy517dw="/>
                                    <field:display field="normalizerId" id="s_com_nw_domain_ColumnDT_normalizerId" object="${columndt}" z="l1exsc72eS72m7p+9GaSZ0uVXEo="/>
                                    </div>
                                </page:show>

                            </div>
                        </div>

                    </section>
                </div>
            </div>

        </div>
    </section>
</div>   