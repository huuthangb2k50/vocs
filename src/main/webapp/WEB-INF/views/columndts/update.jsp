<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
    <section class="content">
        <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input name="q" class="form-control" placeholder="Search..." type="text">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                    <span  id="context" data-toggle="context" data-target="#context-menu">
                                        Offer Templates
                                    </span>
                                </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                            <span  id="context" data-toggle="context" data-target="#context-menu">
                                                MI Templates
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>

                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                    <!-- Content Header (Page header) -->
                    <!-- InstanceBeginEditable name="EditRegion1" -->
                    <section class="content-header">
                        <h1>Update Columndts</h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </section>
                    <section class="content">
                        <div class="box">

                            <!-- /.box-header -->
                            <div class="box-body table-responsive">
                                <form:update id="fu_com_nw_domain_ColumnDT" idField="columnId" modelAttribute="columnDT" path="/columndts" versionField="Version" z="V5bhx2RDmNNuBPoYBpLFCi8zdsI=">
                                    <div class="col-lg-6">
                                    <field:input field="attribute" id="c_com_nw_domain_ColumnDT_attribute" z="IjyCOz/eRmOIsqYUI59uZf2frME="/>
                                    <field:input field="columnIndex" id="c_com_nw_domain_ColumnDT_columnIndex" validationMessageCode="field_invalid_integer" z="ZxAt9VWvTA0Lyoj1Yx4K0+ZkrjQ="/>
                                    </div>
                                    <div class="col-lg-6">
                                    <field:input field="columnName" id="c_com_nw_domain_ColumnDT_columnName" z="8g7KEMvYDZAgnjUR7K5PRZP6k48="/>
                                    <field:select field="normalizerId" id="c_com_nw_domain_ColumnDT_normalizerId" itemValue="normalizerId" items="${normalizers}" path="/normalizers" z="Q3SXIzhhvm0HqDWfG5UN/HRd+XU="/>
                                    </div>
                                </form:update>

                            </div>
                        </div>

                    </section>
                </div>
            </div>

        </div>
    </section>
</div>   