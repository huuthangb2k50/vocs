<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Create triggerMsg</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">triggerMsg</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
    <form:create id="fc_com_nw_domain_TriggerMsg" modelAttribute="triggerMsg" path="/triggermsgs" render="${empty dependencies}" z="XfTQDPwb71+FFXvohS2zDNhBWEU=">
        <div class="col-xs-6">
        <field:input field="msgName" id="c_com_nw_domain_TriggerMsg_msgName" required="true" z="ATAGMtgaN8oNQ7xHrU5wp6AhtOs="/>
        <field:input field="msgDesc" id="c_com_nw_domain_TriggerMsg_msgDesc" z="kMhADlj1QqrlNcokoncLh0nYpwU="/>
        </div>
        <div class="col-xs-6">
        <field:input field="status" id="c_com_nw_domain_TriggerMsg_status" validationMessageCode="field_invalid_integer" z="BQsxK9EM+DLdl8qvKLrWqQ9ovws="/>
        <field:select field="triggerEventId" id="c_com_nw_domain_TriggerMsg_triggerEventId" itemValue="triggerEventId" items="${triggerevents}" path="/triggerevents" z="Mz8sZGErVXOSkxq421KLgYU5X5I="/>
    	</div>
    </form:create>
    <form:dependency dependencies="${dependencies}" id="d_com_nw_domain_TriggerMsg" render="${not empty dependencies}" z="XiTHipiWwK7GTF6vWe7SaEq+mow="/>
</div>
    
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>   