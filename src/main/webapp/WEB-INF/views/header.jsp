<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib tagdir="/WEB-INF/tags/util" prefix="util" %>

	
	<header class="main-header">
    <!-- Logo -->
    <spring:url value="/" var="home" ></spring:url>
    <a href="${home }" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->

      
      <spring:url value="/resources/ocs/dist/img/logo.png" var="logo"></spring:url>
      <span class="logo-mini"><img src="${logo }" width="50px"></span>
      <!-- logo for regular state and mobile devices -->

      <span class="logo-lg"><img src="${logo }" width="120px"></span>

    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-user"></i>&nbsp; Tran Duc Anh
            </a>
            <ul class="dropdown-menu">
            	<li>
                	<ul class="menu">
                    	<li class="header UserAvatar clearfix">
                            <img src="dist/img/avatar5.png" class="img-circle" alt="User Image">
                            <div>
                                <strong>Tran Duc Anh</strong><br>
                                Web Developer
                            </div>
                        </li>
                    	<li><a href="#"><i class="fa fa-user"></i> My Profile</a></li>
              			<li><a href="#"><i class="fa fa-key"></i> Change Password</a></li>
              			<li><a href="#"><i class="fa fa-sign-out"></i> Sight Out</a></li> 
                    </ul>
                </li>
            </ul>
          </li>
          
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="ti-layout-sidebar-right"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  
