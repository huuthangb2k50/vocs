<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Show Zones</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
    <page:show id="ps_com_nw_domain_Threshold" object="${threshold}" path="/thresholds" z="+HpC5vsr9obLpMnENM13AzgZU1c=">
        <field:display field="isPercentage" id="s_com_nw_domain_Threshold_isPercentage" object="${threshold}" z="w7OVYEfHSKVx5dfbs82Hkyqc9SI="/>
        <field:display field="thresholdType" id="s_com_nw_domain_Threshold_thresholdType" object="${threshold}" z="7UoZ9X5y6O0Alj6YcX5W313r9WQ="/>
        <field:display field="thresValue" id="s_com_nw_domain_Threshold_thresValue" object="${threshold}" z="qsffItUBSCBgIlBXhVtLRzef6Pc="/>
        <field:display field="thresholdName" id="s_com_nw_domain_Threshold_thresholdName" object="${threshold}" z="5RZ7+aGezs+Rlf7p09su8qCL6Wo="/>
        <field:display field="externalId" id="s_com_nw_domain_Threshold_externalId" object="${threshold}" z="gmx7iPxaQjRs8NlDiI5X13CQdw0="/>
        <field:display field="triggerId" id="s_com_nw_domain_Threshold_triggerId" object="${threshold}" z="iKYU2kW4iCb5GJlt1ypmJSpjuS0="/>
    </page:show>
    
    </div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>   