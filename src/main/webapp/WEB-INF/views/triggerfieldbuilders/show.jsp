<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Show triggerFieldBuilder</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">triggerFieldBuilder</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
    <page:show id="ps_com_nw_domain_TriggerFieldBuilder" object="${triggerfieldbuilder}" path="/triggerfieldbuilders" z="Z5KZ1acYeEyVgGXAE6zyun1m4vU=">
        <div class="col-xs-6">
        <field:display field="field_name" id="s_com_nw_domain_TriggerFieldBuilder_field_name" object="${triggerfieldbuilder}" z="vfYm+4cZsbBtvxnwOCsIJdQFQDA="/>
        <field:display field="processFunction" id="s_com_nw_domain_TriggerFieldBuilder_processFunction" object="${triggerfieldbuilder}" z="wCssfhZV0Y0/RQuo0BoQslNDIxE="/>
        <field:display field="formatTemplateId" id="s_com_nw_domain_TriggerFieldBuilder_formatTemplateId" object="${triggerfieldbuilder}" z="5MgPpimj6DQjP3A1s37lvD7Y284="/>
        </div>
        <div class="col-xs-6">
        <field:display field="langId" id="s_com_nw_domain_TriggerFieldBuilder_langId" object="${triggerfieldbuilder}" z="S2MZfj67GWGA6NkJEoV6DAzlIJ0="/>
        <field:display field="status" id="s_com_nw_domain_TriggerFieldBuilder_status" object="${triggerfieldbuilder}" z="Iyjq4c2bpopSRy6wcmOTvVWVj0E="/>
    	</div>
    </page:show>
    
    
    </div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>   