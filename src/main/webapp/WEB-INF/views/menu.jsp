<%@page import="java.text.SimpleDateFormat"%>
<%@taglib tagdir="/WEB-INF/tags/menu" prefix="menu"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags/menu" prefix="menu" %>
<%@page contentType="text/html; charset=UTF8" %>
<%-- <%@taglib uri="http://www.springframework.org/security/tags" prefix="security" %> --%>

      <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
    
      <!-- expand submenu-->
      <div class="Menu-expand" align="right">
          <a href="#" onclick="showSubmenu()" class="" data-toggle="" role="button">
            <i class="fa fa-columns"></i>
          </a>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <!-- Optionally, you can add icons to the links -->
        <li><a href="#"><i class="fa fa-area-chart"></i> <span>Dashboard</span></a></li>

        <li><a href="#"><i class="fa fa-gear"></i> <span>System</span>
        	<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
            </a>
            <ul class="treeview-menu">
                <spring:url value="#" var="var"></spring:url>
                <li><a href="${var}"><i class="fa fa-caret-right"></i>System Info</a></li>
                				
                <spring:url value="#" var="var"></spring:url>
                <li><a href="${var}"><i class="fa fa-caret-right"></i>System Configuration</a></li>
                				
                <spring:url value="#" var="var"></spring:url>
                <li><a href="${var}"><i class="fa fa-caret-right"></i>Gateway Configuration</a></li>
                
                <spring:url value="#" var="var"></spring:url>
                <li><a href="${var}"><i class="fa fa-caret-right"></i>Online Charging Module</a></li>
                
                <spring:url value="#" var="var"></spring:url>
                <li><a href="${var}"><i class="fa fa-caret-right"></i>Recurring Process</a></li>
                
                <spring:url value="#" var="var"></spring:url>
                <li><a href="${var}"><i class="fa fa-caret-right"></i>Provisioning Process</a></li>
                
                <spring:url value="#" var="var"></spring:url>
                <li><a href="${var}"><i class="fa fa-caret-right"></i>Domain Management</a></li>
            </ul>
        </li>
        <li><a href="#"><i class="fa fa-leanpub"></i> <span>Catalog Management</span>
        	<span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        
        	<ul class="treeview-menu">

                    <spring:url value="#" var="var"></spring:url>
                	<li><a href="${var}"><i class="fa fa-caret-right"></i>Interfaces
	                		<span class="pull-right-container">
				              <i class="fa fa-angle-left pull-right"></i>
				            </span>
                		</a>
                		
                		<ul class="treeview-menu">
                		
                				<spring:url value="#" var="var"></spring:url>
                				<li><a href="${var}"><i class="fa fa-caret-right"></i>Diameter
	                				<span class="pull-right-container">
						              <i class="fa fa-angle-left pull-right"></i>
						            </span>
                				
                				</a>
                					<ul class="treeview-menu">
                					
                						<spring:url value="#" var="var"></spring:url>
                						<li><a href="${var}"><i class="fa fa-caret-right"></i>AVP Define</a></li>
                				
                						<spring:url value="#" var="var"></spring:url>
                						<li><a href="${var}"><i class="fa fa-caret-right"></i>Basic Types</a></li>
                						
                					</ul>
                				
                				</li>
                				
                				<spring:url value="#" var="var"></spring:url>
                				<li><a href="${var}"><i class="fa fa-caret-right"></i>MML</a></li>
                				
                				<spring:url value="#" var="var"></spring:url>
                				<li><a href="${var}"><i class="fa fa-caret-right"></i>Internal Msg</a></li>
                		</ul>
                	</li>     
                 	
                 	<spring:url value="/baltypes?page=1&amp;size=${empty param.size ? 10 : param.size}" var="baltypes"></spring:url>
                	<li><a href="${baltypes}"><i class="fa fa-caret-right"></i>Balances & Meters</a></li>
                	
                	<spring:url value="/parameterses?page=1&amp;size=${empty param.size ? 10 : param.size}" var="parameter"></spring:url>
                	<li><a href="${parameter}"><i class="fa fa-caret-right"></i>Parameters</a></li>
                	
                	<spring:url value="#" var="var"></spring:url>
                	<li><a href="${var}"><i class="fa fa-caret-right"></i>Triggers
                			<span class="pull-right-container">
				              <i class="fa fa-angle-left pull-right"></i>
				            </span>
                		</a>
                		
                		<ul class="treeview-menu">
                		
                				<spring:url value="/triggertypes?page=1&amp;size=${empty param.size ? 10 : param.size}" var="triggertypes"></spring:url>
                				<li><a href="${triggertypes}"><i class="fa fa-caret-right"></i>Trigger</a></li>
                				
                				<spring:url value="/triggermsgs?page=1&amp;size=${empty param.size ? 10 : param.size}" var="triggermsgs"></spring:url>
                				<li><a href="${triggermsgs}"><i class="fa fa-caret-right"></i>Trigger Message</a></li>
                				
                				<spring:url value="/triggerdestinations?page=1&amp;size=${empty param.size ? 10 : param.size}" var="triggerdestinations"></spring:url>
                				<li><a href="${triggerdestinations}"><i class="fa fa-caret-right"></i>Trigger Destination</a></li>
                		</ul>
                	
                	</li>
                	
                	<spring:url value="#" var="var"></spring:url>
                	<li><a href="${var}"><i class="fa fa-caret-right"></i>Services</a></li>
                	
                	<spring:url value="#" var="var"></spring:url>
                	<li><a href="${var}"><i class="fa fa-caret-right"></i>CDR</a></li>
                	
                	<spring:url value="/zonemaps?form" var="zonedata"></spring:url>
                	<li><a href="${zonedata}"><i class="fa fa-caret-right"></i>Zone Data</a></li>
                	
                	<spring:url value="/geohomezones?page=1&amp;size=${empty param.size ? 10 : param.size}" var="geohomezone"></spring:url>
                	<li><a href="${geohomezone}"><i class="fa fa-caret-right"></i>Geo Home Zone</a></li>
                	
                	<spring:url value="/billingcycles?page=1&amp;size=${empty param.size ? 10 : param.size}" var="billingcycle"></spring:url>
                	<li><a href="${billingcycle}"><i class="fa fa-caret-right"></i>Billing Cycle</a></li>
                	
                	<spring:url value="/unittypes?page=1&amp;size=${empty param.size ? 10 : param.size}" var="unittype"></spring:url>
                	<li><a href="${unittype}"><i class="fa fa-caret-right"></i>Unit Type</a></li>
                	
                	<spring:url value="#" var="var"></spring:url>
                	<li><a href="${var}"><i class="fa fa-caret-right"></i>State Set</a></li>
              	
            	</ul>
        </li>
         <li class="treeview">
          <a href="#"><i class="fa fa-shopping-cart active"></i> <span>Offer Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul style="display: none;" class="treeview-menu cat2">
          	<spring:url value="/offers?form" var="offer"></spring:url>
          	<li><a href="${offer}"><i class="fa fa-caret-right"></i> Offers</a></li>
          	

          	<spring:url value="/eventactiontypemaps?page=1&amp;size=${empty param.size ? 10 : param.size}" var="eventactiontypemaps"></spring:url>
            <li><a href="${eventactiontypemaps }"><i class="fa fa-caret-right"></i> Event & Action type</a></li>
            
            <spring:url value="/actionses?page=1&amp;size=${empty param.size ? 10 : param.size}" var="actionses"></spring:url>
            <li><a href="${actionses }"><i class="fa fa-caret-right"></i> Action </a></li>
            
            <spring:url value="/dynamicreserves?page=1&amp;size=${empty param.size ? 10 : param.size}" var="dynamicserve"></spring:url>      
            <li><a href="${dynamicserve}"><i class="fa fa-caret-right"></i> Dynamic Reservers</a></li>
            
            <spring:url value="/sortpricecomponents?page=1&amp;size=${empty param.size ? 10 : param.size}" var="sortpricecomponent"></spring:url>
            <li><a href="${sortpricecomponent}"><i class="fa fa-caret-right"></i>Sort Price Components</a></li>
            
            <li><a href="#"><i class="fa fa-caret-right"></i> Post Process </a></li>
            
            <spring:url value="/pricecomponents?page=1&amp;size=${empty param.size ? 10 : param.size}" var="pricecomponent"></spring:url>
            <li><a href="${pricecomponent}"><i class="fa fa-caret-right"></i> Price Components</a></li>
            
            <spring:url value="/blocks?page=1&amp;size=${empty param.size ? 10 : param.size}" var="block"></spring:url>
            <li><a href="${block}"><i class="fa fa-caret-right"></i>Blocks</a></li>
            
            <spring:url value="/ratetables?page=1&amp;size=${empty param.size ? 10 : param.size}" var="ratetable"></spring:url>

            <li><a href="${ratetable }"><i class="fa fa-caret-right"></i> Rate tables </a></li>

            
            <spring:url value="/decisiontables?page=1&amp;size=${empty param.size ? 10 : param.size}" var="decisiontable"></spring:url>
            <li><a href="${decisiontable}"><i class="fa fa-caret-right"></i>Decision Tables</a></li>
                	
            <spring:url value="/normalizers?page=1&amp;size=${empty param.size ? 10 : param.size}" var="normalizer"></spring:url>
            <li><a href="${normalizer}"><i class="fa fa-caret-right"></i>Normalizers</a></li>
               
          </ul>
        </li>
        <li><a href="#"><i class="fa fa-book"></i> <span>Policies</span>
	        	<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
            </a>
            <ul class="treeview-menu">
                <spring:url value="#" var="var"></spring:url>
                <li><a href="${var}"><i class="fa fa-caret-right"></i>Policy Profile</a></li>
                				
                <spring:url value="#" var="var"></spring:url>
                <li><a href="${var}"><i class="fa fa-caret-right"></i>PPC Rule</a></li>
                				
                <spring:url value="#" var="var"></spring:url>
                <li><a href="${var}"><i class="fa fa-caret-right"></i>QoS</a></li>
                
                <spring:url value="#" var="var"></spring:url>
                <li><a href="${var}"><i class="fa fa-caret-right"></i>Monitor Key</a></li>
                
                <spring:url value="#" var="var"></spring:url>
                <li><a href="${var}"><i class="fa fa-caret-right"></i>Policy Rules</a></li>
            </ul>
        </li>
        <li class="treeview">
          <a href="#"><i class="fa fa-magic"></i> <span>Tools</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu cat2">
            <li class="treeview">
            	<a href="#"><i class="fa fa-caret-right"></i> Utility</a>
            </li>
            <li><a href="#"><i class="fa fa-caret-right"></i> Modules Management</a></li>
            <li><a href="#"><i class="fa fa-caret-right"></i> Import/Export Data</a></li>
            <li><a href="#"><i class="fa fa-caret-right"></i> Backup/Restore</a></li>
          </ul>
        </li>
        <li><a href="#"><i class="fa fa-wrench"></i> <span>Settings</span>
	        	<span class="pull-right-container">
	              <i class="fa fa-angle-left pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu cat2">
	            <li class="treeview">
	            	<a href="#"><i class="fa fa-caret-right"></i> User Profile</a>
	            </li>
	            <li><a href="#"><i class="fa fa-caret-right"></i> Settings</a></li>
	            <li><a href="#"><i class="fa fa-caret-right"></i> Users Management</a></li>
	            <li><a href="#"><i class="fa fa-caret-right"></i> Menu & Toolbars</a></li>
	            <li><a href="#"><i class="fa fa-caret-right"></i> Log & Audit</a></li>
	          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>




<div id="context-menu" class="">
    <ul class="dropdown-menu" role="menu">
        <li><a href="" tabindex="-1"><i class="fa fa-file-o"></i> New Category</a></li>
        <li><a href="" tabindex="-1"><i class="fa fa-i-cursor"></i> Rename</a></li>
        <li><a href="" tabindex="-1"><i class="fa fa-arrow-circle-o-up"></i> Move up</a></li>
        <li class="divider"></li>
        <li><a href="" tabindex="-1"><i class="fa fa-arrow-circle-o-down"></i> Move down</a></li>
        <li><a href="" tabindex="-1"><i class="fa fa-plus-square-o"></i> Add new child</a></li>
        <li><a href="" tabindex="-1"><i class="fa fa-trash"></i> Remove Category</a></li>
        <li><a href="" tabindex="-1"><i class="fa fa-copy "></i> Coppy</a></li>
    </ul>
</div>
