<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib tagdir="/WEB-INF/tags/util" prefix="util" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                        <ul>
                                            <li>
                                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ion-xbox"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">Deploying</span></a>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-timer"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">MI_Ver_1.0</span></a>
                                                         <ul>
                                                            <li>
                                                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class=" ti-write"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">Register/Purchase</span></a>
                                                                <ul>
                                                                    <li>
                                                                        <span><i class="fa fa-minus-square-o"></i></span><a href=""><i class="ion-social-markdown"></i> <span  id="context" data-toggle="context" data-target="#context-menu"> MI Purchase Componence</span></a>
                                                                        <ul>
                                                                            <li>
                                                                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""> <i class="ti-package"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">MI Block</span></a>
                                                                                <ul>
                                                                                    <li><span><i class="fa fa-share-alt"></i></span> <a href=""> <span  id="context" data-toggle="context" data-target="#context-menu">Rate Table for Purchase</span></a></li>
                                                                                    <li><span><i class="fa fa-share-alt"></i></span> <a href=""> <span  id="context" data-toggle="context" data-target="#context-menu">Rate Table 2</span></a></li>
                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                                
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-microphone"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         Voice Templates
                                         </span>
                                         </a>
                                        <ul>
                                                    <li>
                                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-timer"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">MI_Ver_1.0</span></a>
                                                         <ul>
                                                            <li>
                                                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class=" ti-write"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">Register/Purchase</span></a>
                                                                <ul>
                                                                    <li>
                                                                        <span><i class="fa fa-minus-square-o"></i></span><a href=""><i class="ion-social-markdown"></i> <span  id="context" data-toggle="context" data-target="#context-menu"> MI Purchase Componence</span></a>
                                                                        <ul>
                                                                            <li>
                                                                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""> <i class="ti-package"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">MI Block</span></a>
                                                                                <ul>
                                                                                    <li><span><i class="fa fa-share-alt"></i></span> <a href=""> <span  id="context" data-toggle="context" data-target="#context-menu">Rate Table for Purchase</span></a></li>
                                                                                    <li><span><i class="fa fa-share-alt"></i></span> <a href=""> <span  id="context" data-toggle="context" data-target="#context-menu">Rate Table 2</span></a></li>
                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                                
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-reload"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Subcription Offers
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-medall"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         Main Offer
                                         </span>
                                         </a>
                                        <ul>
                                            <li>
                                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="fa fa-empire"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">Tomato</span></a>
                                          </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-medall-alt"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         Nomarl Offer
                                         </span>
                                         </a>
                                        <ul>
                                                    <li>
                                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-timer"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">MI_Ver_1.0</span></a>
                                                         <ul>
                                                            <li>
                                                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class=" ti-write"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">Register/Purchase</span></a>
                                                                <ul>
                                                                    <li>
                                                                        <span><i class="fa fa-minus-square-o"></i></span><a href=""><i class="ion-social-markdown"></i> <span  id="context" data-toggle="context" data-target="#context-menu"> MI Purchase Componence</span></a>
                                                                        <ul>
                                                                            <li>
                                                                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""> <i class="ti-package"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">MI Block</span></a>
                                                                                <ul>
                                                                                    <li><span><i class="fa fa-share-alt"></i></span> <a href=""> <span  id="context" data-toggle="context" data-target="#context-menu">Rate Table for Purchase</span></a></li>
                                                                                    <li><span><i class="fa fa-share-alt"></i></span> <a href=""> <span  id="context" data-toggle="context" data-target="#context-menu">Rate Table 2</span></a></li>
                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                                
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
                        
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-time"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 One time Offers
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-medall"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         Main Offer
                                         </span>
                                         </a>
                                        <ul>
                                            <li>
                                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="fa ion-xbox"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">Tomato</span></a>
                                          </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-medall-alt"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         Nomarl Offer
                                         </span>
                                         </a>
                                        <ul>
                                            <li>
                                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ion-xbox"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">Deploying</span></a>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-timer"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">MI_Ver_1.0</span></a>
                                                         <ul>
                                                            <li>
                                                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class=" ti-write"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">Register/Purchase</span></a>
                                                                <ul>
                                                                    <li>
                                                                        <span><i class="fa fa-minus-square-o"></i></span><a href=""><i class="ion-social-markdown"></i> <span  id="context" data-toggle="context" data-target="#context-menu"> MI Purchase Componence</span></a>
                                                                        <ul>
                                                                            <li>
                                                                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""> <i class="ti-package"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">MI Block</span></a>
                                                                                <ul>
                                                                                    <li><span><i class="fa fa-share-alt"></i></span> <a href=""> <span  id="context" data-toggle="context" data-target="#context-menu">Rate Table for Purchase</span></a></li>
                                                                                    <li><span><i class="fa fa-share-alt"></i></span> <a href=""> <span  id="context" data-toggle="context" data-target="#context-menu">Rate Table 2</span></a></li>
                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                                
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
                        
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="fa fa-object-group"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer groups
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-medall"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         Main Offer
                                         </span>
                                         </a>
                                        <ul>
                                            <li>
                                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="fa ion-xbox"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">Tomato</span></a>
                                          </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-medall-alt"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         Nomarl Offer
                                         </span>
                                         </a>
                                        <ul>
                                            <li>
                                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ion-xbox"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">Deploying</span></a>
                                                <ul>
                                                    <li>
                                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-timer"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">MI_Ver_1.0</span></a>
                                                         <ul>
                                                            <li>
                                                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class=" ti-write"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">Register/Purchase</span></a>
                                                                <ul>
                                                                    <li>
                                                                        <span><i class="fa fa-minus-square-o"></i></span><a href=""><i class="ion-social-markdown"></i> <span  id="context" data-toggle="context" data-target="#context-menu"> MI Purchase Componence</span></a>
                                                                        <ul>
                                                                            <li>
                                                                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""> <i class="ti-package"></i>  <span  id="context" data-toggle="context" data-target="#context-menu">MI Block</span></a>
                                                                                <ul>
                                                                                    <li><span><i class="fa fa-share-alt"></i></span> <a href=""> <span  id="context" data-toggle="context" data-target="#context-menu">Rate Table for Purchase</span></a></li>
                                                                                    <li><span><i class="fa fa-share-alt"></i></span> <a href=""> <span  id="context" data-toggle="context" data-target="#context-menu">Rate Table 2</span></a></li>
                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                                
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                    <section class="content-header">
                      <h1> Dashboard </h1>
                      <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                  </section>
                  <section class="content">
                  	<div class="row">

                  	<spring:url value="/resources/ocs/dist/img/map.jpg" var="map"></spring:url>
                    	<div class="col-md-8"><img src="${map }"></div>

                    	<div class="col-md-4">
                              
                              <h4>Number of objects:</h4>
                              <!-- /.progress-group -->
                              <div class="progress-group">
                                <span class="progress-text">Offers:</span>
                                <span class="progress-number"><b>310</b>/400</span>
            

                                <div class="progress sm">
                                  <div class="progress-bar progress-bar-red" style="width: 80%"></div>
                                </div>
                              </div>
                              <!-- /.progress-group -->
                              <div class="progress-group">
                                <span class="progress-text">Active Offers:</span>
                                <span class="progress-number"><b>310</b>/400</span>
            
                                <div class="progress sm">
                                  <div class="progress-bar progress-bar-green" style="width: 80%"></div>
                                </div>
                              </div>
                              <!-- /.progress-group -->
                              <div class="progress-group">
                                <span class="progress-text">Suspend Offer:</span>
                                <span class="progress-number"><b>310</b>/400</span>
            
                                <div class="progress sm">
                                  <div class="progress-bar progress-bar-green" style="width: 80%"></div>
                                </div>
                              </div>
                              <h4>Main site:</h4>
                              <!-- Info Boxes Style 2 -->
                              <div class="info-box bg-red">
                                <span class="info-box-icon"><i class="fa fa-database"></i></span>
                    
                                <div class="info-box-content">
                                  <span class="info-box-text">My SQL Database:</span>
                                  <span class="info-box-number">5,200</span>
                    
                                  <div class="progress">
                                    <div class="progress-bar" style="width: 50%"></div>
                                  </div>
                                      <span class="progress-description">
                                        50% Increase in 30 Days
                                      </span>
                                </div>
                                <!-- /.info-box-content -->
                              </div>
                              <h4>DR Site:</h4>
                              <!-- Info Boxes Style 2 -->
                              <div class="info-box bg-blue">
                                <span class="info-box-icon"><i class="fa fa-database"></i></span>
                    
                                <div class="info-box-content">
                                  <span class="info-box-text">My SQL Database:</span>
                                  <span class="info-box-number">5,200</span>
                    
                                  <div class="progress">
                                    <div class="progress-bar" style="width: 50%"></div>
                                  </div>
                                      <span class="progress-description">
                                        50% Increase in 30 Days
                                      </span>
                                </div>
                                <!-- /.info-box-content -->
                              </div>
                              
                              
                              
                        </div>
                    </div>
                  	
                  </section>
                  
                <!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->

    </div>



