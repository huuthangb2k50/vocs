<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Show Zones</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
    <page:show id="ps_com_nw_domain_Offer" object="${offer}" path="/offers" z="UcwqGW/4+q+Pa7QXwmVWf3zuMTA=">
        <div class="col-xs-6">
        <field:display field="offerName" id="s_com_nw_domain_Offer_offerName" object="${offer}" z="gQPIz7qbvevXFCsKwmvJzLVUngI="/>
        <field:display field="categoryId" id="s_com_nw_domain_Offer_categoryId" object="${offer}" z="mze1qICtabA3yNUKzxVyEEvoO5A="/>
        <field:display field="priority" id="s_com_nw_domain_Offer_priority" object="${offer}" z="Ix8Iiah58cHm4VTRPDrN0u5g3vE="/>
        <field:display date="true" dateTimePattern="${offer_effdate_date_format}" field="effDate" id="s_com_nw_domain_Offer_effDate" object="${offer}" z="o4vKuJ1k7F8XVrgp4IOib0zDmiM="/>
        <field:display date="true" dateTimePattern="${offer_expdate_date_format}" field="expDate" id="s_com_nw_domain_Offer_expDate" object="${offer}" z="39fp5wuBCYorAUuj5jJ7CqG03xo="/>
        <field:display field="createType" id="s_com_nw_domain_Offer_createType" object="${offer}" z="iy6wHPRjzy/tOTtcp/gdAj6N2rM="/>
        <field:display field="offerType" id="s_com_nw_domain_Offer_offerType" object="${offer}" z="bpmCqKbQXhveYf21Z2ihQ9fkqXU="/>
        </div>
        <div class="col-xs-6">
        <field:display field="billingCycleTypeId" id="s_com_nw_domain_Offer_billingCycleTypeId" object="${offer}" z="rpE9hzv77zwYJFcZ9paDjjshmJU="/>
        <field:display field="offerExternalId" id="s_com_nw_domain_Offer_offerExternalId" object="${offer}" z="dXj8nWC6r8tyyu9+RFpsuYB6IFU="/>
        <field:display field="versionInfo" id="s_com_nw_domain_Offer_versionInfo" object="${offer}" z="YnpRttNlUFaLBT03LOLbuqQlsV8="/>
        <field:display field="specialCalMethod" id="s_com_nw_domain_Offer_specialCalMethod" object="${offer}" z="y8dMjJ4pNwOLbmE8hRZNrpgP7Cw="/>
        <field:display field="offerTemplateId" id="s_com_nw_domain_Offer_offerTemplateId" object="${offer}" z="Ha5KYQD3jeMlkGNESMKWmFO77So="/>
        <field:display field="description" id="s_com_nw_domain_Offer_description" object="${offer}" z="29uZvOqRZdwYAGdbw1y4XE726bc="/>
        <field:display field="offerCycle" id="s_com_nw_domain_Offer_offerCycle" object="${offer}" z="jVUDajhFW1eE7pPmZmJWcFQRgA8="/>
    	</div>
    </page:show>
    
    </div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>   