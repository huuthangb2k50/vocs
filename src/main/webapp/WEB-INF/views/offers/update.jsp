<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
  	<section class="content">
    <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input name="q" class="form-control" placeholder="Search..." type="text">
                              <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </span>
                        </div>
                  </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                 <span  id="context" data-toggle="context" data-target="#context-menu">
                                 Offer Templates
                                 </span>
                                 </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                         <span  id="context" data-toggle="context" data-target="#context-menu">
                                         MI Templates
                                         </span>
                                         </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
    
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                <!-- Content Header (Page header) -->
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <section class="content-header">
                    <h1>Update Zones</h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="box">
                        
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
    <form:update id="fu_com_nw_domain_Offer" idField="offerId" modelAttribute="offer" path="/offers" versionField="Version" z="lf0NWZ/exdQKOxdj7ak4XBcWMg4=">
        <div class="col-xs-6">
        <field:input field="offerName" id="c_com_nw_domain_Offer_offerName" z="jOikH8zch4CJQ4uXKAaazhRIq9w="/>
        <field:select field="categoryId" id="c_com_nw_domain_Offer_categoryId" itemValue="categoryId" items="${categorys}" path="/categorys" z="Um194rcXQVKrJ7frS8izj+wy4Ug="/>
        <field:input field="priority" id="c_com_nw_domain_Offer_priority" validationMessageCode="field_invalid_integer" z="S8DKSDx1FenV+ZiJ7WVEdSvat3A="/>
        <field:datetime dateTimePattern="${offer_effdate_date_format}" field="effDate" id="c_com_nw_domain_Offer_effDate" z="4P5h7+4Rqf+dL4gRoKspO+glAI4="/>
        <field:datetime dateTimePattern="${offer_expdate_date_format}" field="expDate" id="c_com_nw_domain_Offer_expDate" z="ay01chd3zTSvBnrmK/r5/iY/JwM="/>
        <field:input field="createType" id="c_com_nw_domain_Offer_createType" validationMessageCode="field_invalid_integer" z="MclR3wf8d2jX5yFcpKNFN7xNHYs="/>
        <field:input field="offerType" id="c_com_nw_domain_Offer_offerType" validationMessageCode="field_invalid_integer" z="rlZL79swT1d35eu1tR+3Emng4i0="/>
        </div>
        <div class="col-xs-6">
        <field:select field="billingCycleTypeId" id="c_com_nw_domain_Offer_billingCycleTypeId" itemValue="billingCycleTypeId" items="${billingcycletypes}" path="/billingcycletypes" z="iqp4Fy7fqlFTCNJcK/KzqMGv7R0="/>
        <field:input field="offerExternalId" id="c_com_nw_domain_Offer_offerExternalId" z="+dB1q62ncHbz720b/KJkZ9b7yow="/>
        <field:input field="versionInfo" id="c_com_nw_domain_Offer_versionInfo" z="gEmAz6uVTUeO0jjN0wowTkPSRFo="/>
        <field:input field="specialCalMethod" id="c_com_nw_domain_Offer_specialCalMethod" validationMessageCode="field_invalid_integer" z="6Yf3OhPzXgusS1skAclITVhY+n8="/>
        <field:input field="offerTemplateId" id="c_com_nw_domain_Offer_offerTemplateId" validationMessageCode="field_invalid_integer" z="0hmz4Bd8dj8bZVAgiQb7VfgdLR4="/>
        <field:input field="description" id="c_com_nw_domain_Offer_description" z="SGB4sPYGFelhzOXKTQNVhSd+xHo="/>
        <field:input field="offerCycle" id="c_com_nw_domain_Offer_offerCycle" validationMessageCode="field_invalid_integer" z="Bhit/rpWeY7iglWWdW0oKrVv+gQ="/>
    	</div>
    </form:update>
    
    					</div>
                        <!-- /.box-body -->
                      </div>
          			<!-- /.box -->
          			
                </section>
            	<!-- InstanceEndEditable -->
                </div>
           </div>
            
        </div>
    </section>
    <!-- /.content -->
  </div>   