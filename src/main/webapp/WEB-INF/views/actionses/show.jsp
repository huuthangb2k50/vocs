<%@taglib tagdir="/WEB-INF/tags/form" prefix="page" %>
<%@taglib tagdir="/WEB-INF/tags/form" prefix="form" %>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="table"%>
<%@taglib tagdir="/WEB-INF/tags/form/fields"  prefix="field"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="content-wrapper">
    <section class="content">
        <!-- Main content -->
        <div id="row_submenuRight" class="row submenuRight ShowMenu">
            <!-- SubleftMenu -->
            <div class="col-xs-3 HideMenu">
                <div class="Submenu">
                    <h3>Rate Tables<span onClick="hideSubmenu()" class="closeLink pull-right"><a  href="#"><i  class="ion-ios-close-empty"></i></a></span></h3>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input name="q" class="form-control" placeholder="Search..." type="text">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <div id="Treemenu1" class="tree clearfix">
                        <ul class="dad">
                            <li>
                                <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-shopping-cart-full"></i>
                                    <span  id="context" data-toggle="context" data-target="#context-menu">
                                        Offer Templates
                                    </span>
                                </a>
                                <ul>
                                    <li>
                                        <span><i class="fa fa-minus-square-o"></i></span> <a href=""><i class="ti-gallery"></i>
                                            <span  id="context" data-toggle="context" data-target="#context-menu">
                                                MI Templates
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>

                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="col-xs-9">
                <div class="row">
                    <!-- Content Header (Page header) -->
                    <!-- InstanceBeginEditable name="EditRegion1" -->
                    <section class="content-header">
                        <h1>Show Actionses</h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </section>
                    <section class="content">
                        <div class="box">

                            <!-- /.box-header -->
                            <div class="box-body table-responsive">
                                <page:show id="ps_com_nw_domain_Actions" object="${actions}" path="/actionses" z="xqB0rymUJ9Dbz9Z4P5C5nk764wE=">
                                    <div class="col-lg-6">
                                        <field:display field="actionName" id="s_com_nw_domain_Actions_actionName" object="${actions}" z="fk1xx3AkXnI8cGUHpJ8nbv3gO70="/>
                                        <field:display field="actionType" id="s_com_nw_domain_Actions_actionType" object="${actions}" z="RrQQDWBJ5N9ZMMW3JjsjWXGTqL0="/>
                                        <field:display field="category_id" id="s_com_nw_domain_Actions_category_id" object="${actions}" z="x8PYkK0x8w4UrgiWpTK0FdJPKoM="/>
                                    </div>
                                    <div class="col-lg-6">
                                        <field:display field="sortPriceComponentId" id="s_com_nw_domain_Actions_sortPriceComponentId" object="${actions}" z="UKs/q3wGWiW6m1vWE+q9JCgcNSs="/>
                                        <field:display field="dynamicReserveId" id="s_com_nw_domain_Actions_dynamicReserveId" object="${actions}" z="F/5r4IEa+iz5YHUnSUrq+uXeh5A="/>
                                    </div>
                                </page:show>


                            </div>
                        </div>

                    </section>
                </div>
            </div>

        </div>
    </section>
</div>   