package com.nw.domain.finder;

import java.util.List;

import javax.persistence.TypedQuery;

import com.nw.domain.NestedSet;

public class NestedSetFinder {

	public static NestedSet findRootByTreeType(int treeType) {
		TypedQuery<NestedSet> temp = NestedSet
				.findNestedSetsByTreeTypeAndTargetNameIsNull(treeType);
		return temp.getSingleResult();
	}

	public static List<NestedSet> findChildrenByTreeTypeAndId(int treeType, long id) {
		TypedQuery<NestedSet> temp = NestedSet.findChildrenNestedSetsByTreeTypeAndId(treeType, id);
		return temp.getResultList();
	}

}
