package com.nw.domain;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.roo.addon.json.RooJson;

@RooJavaBean
@RooToString
@RooJson
@RooJpaActiveRecord(sequenceName = "seq_nested_set", identifierColumn = "id", identifierField = "id", table = "nested_set", 
					finders = { "findNestedSetsByTreeTypeAndTargetNameIsNull" })
public class NestedSet {

    @Column(name = "target_id")
    private long targetId;

    @Column(name = "target_name")
    private String targetName;

    @Column(name = "node_name")
    private String nodeName;

    @Column(name = "tree_type")
    private int treeType;

    @Column(name = "lft")
    private int lft;

    @Column(name = "rgt")
    private int rgt;
    
    public static TypedQuery<NestedSet> findChildrenNestedSetsByTreeTypeAndId(int treeType, long id) {
    	StringBuilder query = new StringBuilder();
    	query.append("SELECT child ");
    	query.append("FROM NestedSet AS parent, NestedSet AS child ");
    	query.append("WHERE parent.id = :id AND parent.treeType = :treeType AND child.treeType = :treeType ");
    	query.append("AND child.lft > parent.lft AND child.rgt < parent.rgt ");
    	query.append("AND NOT EXISTS (SELECT tmp FROM NestedSet AS tmp ");
    	query.append("WHERE tmp.treeType = :treeType ");
    	query.append("AND tmp.lft > parent.lft AND tmp.rgt < parent.rgt ");
    	query.append("AND child.lft > tmp.lft AND child.rgt < tmp.rgt) ");
        EntityManager em = NestedSet.entityManager();
        TypedQuery<NestedSet> q = em.createQuery(query.toString(), NestedSet.class);
        q.setParameter("treeType", treeType);
        q.setParameter("id", id);
        return q;
    }
}
