package com.nw.tree.node;

import javassist.NotFoundException;

public enum TreeType {

	NORMALIZER(22, NormalizerTreeNode.class);
	
	private int value;
	private Class classInstance;
	
	private TreeType(int value, Class classInstance) {
		this.value = value;
		this.classInstance = classInstance;
	}
	
	public int getValue() {
		return this.value;
	}
	
	public Class getClassInstance() {
		return this.classInstance;
	}
	
	public static TreeType getByValue(int value) throws NotFoundException {
		for (TreeType treeType : TreeType.values()) {
			if (treeType.getValue() == value) {
				return treeType;
			}
		}
		throw new NotFoundException("No TreeType defined by " + value);
	}
}
