package com.nw.tree.node.target;

import com.nw.domain.Normalizer;

public class NormalizerTreeNodeTarget implements TreeNodeTartget {

	@Override
	public String getNodeName(long targetId) {
		Normalizer normalizer = Normalizer.findNormalizer(targetId);
		if(normalizer != null) {
			return normalizer.getNormalizerName();
		}
		return null;
	}

}
