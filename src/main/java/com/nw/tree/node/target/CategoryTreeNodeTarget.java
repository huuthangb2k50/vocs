package com.nw.tree.node.target;

import com.nw.domain.Category;

public class CategoryTreeNodeTarget implements TreeNodeTartget {

	@Override
	public String getNodeName(long targetId) {
		Category category = Category.findCategory(targetId);
		if(category != null) {
			return category.getCategoryName();
		}
		
		return null;
	}

}
