package com.nw.tree.node.target;

import javassist.NotFoundException;

public enum TargetName {

	CATEGORY("category", CategoryTreeNodeTarget.class),
	NORMALIZER("normalizer", NormalizerTreeNodeTarget.class);

	private String value;
	private Class classInstance;

	private TargetName(String value, Class classInstance) {
		this.value = value;
		this.classInstance = classInstance;
	}

	public String getValue() {
		return this.value;
	}

	public Class getClassInstance() {
		return this.classInstance;
	}

	public static TargetName getByValue(String value) throws NotFoundException {
		for (TargetName targetName : TargetName.values()) {
			if (targetName.getValue().equals(value)) {
				return targetName;
			}
		}
		throw new NotFoundException("No TargetName defined by " + value);
	}
}
