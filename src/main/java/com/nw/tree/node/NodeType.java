package com.nw.tree.node;

import javassist.NotFoundException;

public enum NodeType {

	NORMALIZER_ROOT("NORMALIZER_ROOT", TreeType.NORMALIZER, ""),
	NORMALIZER_CATEGORY("NORMALIZER_CATEGORY", TreeType.NORMALIZER, "category"),
	NORMALIZER_NORMALIZER("NORMALIZER_NORMALIZER", TreeType.NORMALIZER, "normalizer");
	
	private String value;
	private TreeType treeType;
	private String targetName;
	
	private NodeType(String value, TreeType treeType, String targetName) {
		this.value = value;
		this.treeType = treeType;
		this.targetName = targetName;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public TreeType getTreeType() {
		return this.treeType;
	}
	
	public String getTargetName() {
		return this.targetName;
	}
	
	public static NodeType getByTreeTypeAndTargetName(TreeType treeType, String targetName) throws NotFoundException {		
		for (NodeType nodeType : NodeType.values()) {
			if (nodeType.getTreeType().equals(treeType) && nodeType.getTargetName().equals(targetName)) {
				return nodeType;
			}
		}
		throw new NotFoundException("No NodeType defined by " + treeType.getValue() + "-" + targetName);
	}
}
