package com.nw.tree.node;

import java.util.List;

import com.google.gson.Gson;

public abstract class TreeNode {

	public Object getNode() {
		Gson gson = new Gson();
		if (this.id == 0) {
			TreeNode node = this.getRootTreeNode();
			if (node != null) {
				return gson.toJson(node);
			}
		} else {
			List<TreeNode> nodes = this.getChildrenTreeNodes();
			if (nodes != null && nodes.size() > 0) {
				return gson.toJson(nodes);
			}
		}

		return "";
	}

	protected abstract TreeNode getRootTreeNode();

	protected abstract List<TreeNode> getChildrenTreeNodes();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isChildren() {
		return children;
	}

	public void setChildren(boolean children) {
		this.children = children;
	}

	public String getNodeType() {
		return nodeType;
	}

	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}

	public int getTreeType() {
		return treeType;
	}

	public void setTreeType(int treeType) {
		this.treeType = treeType;
	}

	private long id;
	private String text;
	private boolean children;
	private String nodeType;
	private int treeType;
}
