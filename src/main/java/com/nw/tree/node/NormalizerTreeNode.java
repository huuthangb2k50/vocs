package com.nw.tree.node;

import java.util.ArrayList;
import java.util.List;

import javassist.NotFoundException;

import com.nw.domain.NestedSet;
import com.nw.domain.finder.NestedSetFinder;

public class NormalizerTreeNode extends TreeNode {
	
	protected TreeNode getRootTreeNode() {
		
		NestedSet temp = NestedSetFinder.findRootByTreeType(this.getTreeType());
		
		if(temp != null) {
			TreeNode node = new NormalizerTreeNode();
			node.setId(temp.getId());
			node.setText(temp.getNodeName());
			node.setChildren(true);
			node.setNodeType(NodeType.NORMALIZER_ROOT.getValue());
			node.setTreeType(this.getTreeType());
			
			return node;
		}
		
		return null;
	}
	
	protected List<TreeNode> getChildrenTreeNodes() {
		
		List<NestedSet> temps = NestedSetFinder.findChildrenByTreeTypeAndId(this.getTreeType(), this.getId());
		if(temps != null && temps.size() > 0) {
			List<TreeNode> nodes = new ArrayList<TreeNode>();
			for (NestedSet temp : temps) {
				TreeNode node = new NormalizerTreeNode();
				node.setId(temp.getId());
				node.setText(TreeNodeUtil.getNodeName(temp.getTargetName(), temp.getTargetId()));
				node.setChildren(true);
				try {
					NodeType nodeType = NodeType.getByTreeTypeAndTargetName(TreeType.NORMALIZER, temp.getTargetName());
					node.setNodeType(nodeType.getValue());
				} catch (NotFoundException ex) {
					// do nothing
				}
				node.setTreeType(this.getTreeType());
				nodes.add(node);
			}
			
			return nodes;
		}
		
		return null;
	}

}
