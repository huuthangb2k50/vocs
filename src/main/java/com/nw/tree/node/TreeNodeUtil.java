package com.nw.tree.node;

import com.nw.tree.node.target.TargetName;
import com.nw.tree.node.target.TreeNodeTartget;

public class TreeNodeUtil {

	public static TreeNode getInstance(int treeType, int id) {
		try {
			TreeType temp = TreeType.getByValue(treeType);
		
			TreeNode treeNode = (TreeNode) temp.getClassInstance().getConstructors()[0].newInstance();
			treeNode.setTreeType(temp.getValue());
			treeNode.setId(id);
			
			return treeNode;
		} catch(Exception ex) {
			// do nothing
		}
		
		return null;
	}
	
	public static String getNodeName(String value, long targetId) {
		try {
			TargetName temp = TargetName.getByValue(value);
			
			TreeNodeTartget treeNodeTarget = (TreeNodeTartget) temp.getClassInstance().getConstructors()[0].newInstance();
			
			return treeNodeTarget.getNodeName(targetId);
		} catch(Exception ex) {
			// do nothing
		}
		
		return null;
	}
}
