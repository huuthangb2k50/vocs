package com.nw.web;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/ajaxtest/**")
@Controller
public class AjaxTest {

    @RequestMapping(method = RequestMethod.POST, value = "{id}")
    public void post(@PathVariable Long id, ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
    }

    @RequestMapping
    public String index() {
        return "ajaxtest/index";
    }
    
    @ResponseBody
    @RequestMapping(value="/ajaxtest/ajax", method=RequestMethod.POST,produces = "text/plain;charset=UTF-8") 
    public String ajaxCall(){
    	System.out.println("abc");
    	
    	return "{\"status\":\"OK\"}";
    }
    
    
}
