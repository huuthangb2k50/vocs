package com.nw.web;

import com.nw.domain.ZoneMap;
import com.nw.domain.GeoHomeZone;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

@RequestMapping("/zonemaps")
@Controller
@RooWebScaffold(path = "zonemaps", formBackingObject = ZoneMap.class)
public class ZoneMapController {
	
	@ModelAttribute("geohomezones")
	public List<GeoHomeZone> getGeoHomeZone(
							@RequestParam(value = "page", required = false) Integer page,
							@RequestParam(value = "size", required = false) Integer size,
							@RequestParam(value = "sortFieldName", required = false) String sortFieldName,
							@RequestParam(value = "sortOrder", required = false) String sortOrder,
							Model uiModel){
		List<GeoHomeZone> result = new ArrayList<GeoHomeZone>();
		if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            uiModel.addAttribute("result",GeoHomeZone.findAllGeoHomeZones());
            float nrOfPages = (float) GeoHomeZone.countGeoHomeZones() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            //uiModel.addAttribute("result", GeoHomeZone.findAllGeoHomeZones());
        	System.out.println("aaa");
        	page =1; size=1;
        	int sizeNo = size == null ? 10 : size.intValue();
            uiModel.addAttribute("result",GeoHomeZone.findAllGeoHomeZones());
            float nrOfPages = (float) GeoHomeZone.countGeoHomeZones() / sizeNo;
            
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        	
        }
		
		
		//result = GeoHomeZone.findAllGeoHomeZones();
		return result;
	}
	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid ZoneMap zoneMap, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, zoneMap);
            return "zonemaps/create";
        }
        uiModel.asMap().clear();
        zoneMap.persist();
        return "redirect:/zonemaps/" + encodeUrlPathSegment(zoneMap.getZoneMapId().toString(), httpServletRequest);
    }

	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        populateEditForm(uiModel, new ZoneMap());
        return "zonemaps/create";
    }

	@RequestMapping(value = "/{zoneMapId}", produces = "text/html")
    public String show(@PathVariable("zoneMapId") Long zoneMapId, Model uiModel) {
        uiModel.addAttribute("zonemap", ZoneMap.findZoneMap(zoneMapId));
        uiModel.addAttribute("itemId", zoneMapId);
        return "zonemaps/show";
    }

	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, @RequestParam(value = "sortFieldName", required = false) String sortFieldName, @RequestParam(value = "sortOrder", required = false) String sortOrder, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("zonemaps", ZoneMap.findZoneMapEntries(firstResult, sizeNo, sortFieldName, sortOrder));
            float nrOfPages = (float) ZoneMap.countZoneMaps() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("zonemaps", ZoneMap.findAllZoneMaps(sortFieldName, sortOrder));
        }
        return "zonemaps/list";
    }

	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid ZoneMap zoneMap, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, zoneMap);
            return "zonemaps/update";
        }
        uiModel.asMap().clear();
        zoneMap.merge();
        return "redirect:/zonemaps/" + encodeUrlPathSegment(zoneMap.getZoneMapId().toString(), httpServletRequest);
    }

	@RequestMapping(value = "/{zoneMapId}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("zoneMapId") Long zoneMapId, Model uiModel) {
        populateEditForm(uiModel, ZoneMap.findZoneMap(zoneMapId));
        return "zonemaps/update";
    }

	@RequestMapping(value = "/{zoneMapId}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("zoneMapId") Long zoneMapId, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        ZoneMap zoneMap = ZoneMap.findZoneMap(zoneMapId);
        zoneMap.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/zonemaps";
    }

	void populateEditForm(Model uiModel, ZoneMap zoneMap) {
        uiModel.addAttribute("zoneMap", zoneMap);
    }

	String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
}
