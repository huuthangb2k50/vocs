package com.nw.web;
import com.nw.domain.GeoHomeZone;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

@RequestMapping("/geohomezones")
@Controller
@RooWebScaffold(path = "geohomezones", formBackingObject = GeoHomeZone.class)
public class GeoHomeZoneController {

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid GeoHomeZone geoHomeZone, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, geoHomeZone);
            return "geohomezones/create";
        }
        uiModel.asMap().clear();
        geoHomeZone.persist();
        return "redirect:/geohomezones/" + encodeUrlPathSegment(geoHomeZone.getGeoHomeZoneId().toString(), httpServletRequest);
    }

	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        populateEditForm(uiModel, new GeoHomeZone());
        return "geohomezones/create";
    }

	@RequestMapping(value = "/{geoHomeZoneId}", produces = "text/html")
    public String show(@PathVariable("geoHomeZoneId") Long geoHomeZoneId, Model uiModel) {
        uiModel.addAttribute("geohomezone", GeoHomeZone.findGeoHomeZone(geoHomeZoneId));
        uiModel.addAttribute("itemId", geoHomeZoneId);
        return "geohomezones/show";
    }

	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, @RequestParam(value = "sortFieldName", required = false) String sortFieldName, @RequestParam(value = "sortOrder", required = false) String sortOrder, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("geohomezones", GeoHomeZone.findGeoHomeZoneEntries(firstResult, sizeNo, sortFieldName, sortOrder));
            float nrOfPages = (float) GeoHomeZone.countGeoHomeZones() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("geohomezones", GeoHomeZone.findAllGeoHomeZones(sortFieldName, sortOrder));
        }
        return "geohomezones/list";
    }

	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid GeoHomeZone geoHomeZone, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, geoHomeZone);
            return "geohomezones/update";
        }
        uiModel.asMap().clear();
        geoHomeZone.merge();
        return "redirect:/geohomezones/" + encodeUrlPathSegment(geoHomeZone.getGeoHomeZoneId().toString(), httpServletRequest);
    }

	@RequestMapping(value = "/{geoHomeZoneId}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("geoHomeZoneId") Long geoHomeZoneId, Model uiModel) {
        populateEditForm(uiModel, GeoHomeZone.findGeoHomeZone(geoHomeZoneId));
        return "geohomezones/update";
    }

	@RequestMapping(value = "/{geoHomeZoneId}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("geoHomeZoneId") Long geoHomeZoneId, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        GeoHomeZone geoHomeZone = GeoHomeZone.findGeoHomeZone(geoHomeZoneId);
        geoHomeZone.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/geohomezones";
    }

	void populateEditForm(Model uiModel, GeoHomeZone geoHomeZone) {
        uiModel.addAttribute("geoHomeZone", geoHomeZone);
    }

	String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
}
