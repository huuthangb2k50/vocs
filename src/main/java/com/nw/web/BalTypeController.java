package com.nw.web;
import java.util.ArrayList;
import java.util.List;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.nw.domain.BalType;
import com.nw.domain.ThresholdBalTypeMap;

@RequestMapping("/baltypes")
@Controller
@RooWebScaffold(path = "baltypes", formBackingObject = BalType.class)
public class BalTypeController {
	
	@ModelAttribute("BaltypesMapThreshold")
	 public List<ThresholdBalTypeMap> getThresholdBalTypeMap(
	       @RequestParam(value = "page", required = false) Integer page,
	       @RequestParam(value = "size", required = false) Integer size,
	       @RequestParam(value = "sortFieldName", required = false) String sortFieldName,
	       @RequestParam(value = "sortOrder", required = false) String sortOrder,
	       Model uiModel){
	  List<ThresholdBalTypeMap> result = new ArrayList<ThresholdBalTypeMap>();
	  		if (page != null || size != null) {
	            int sizeNo = size == null ? 10 : size.intValue();
	            uiModel.addAttribute("result",ThresholdBalTypeMap.findAllThresholdBalTypeMaps());
	            System.out.println();
	            float nrOfPages = (float) ThresholdBalTypeMap.countThresholdBalTypeMaps() / sizeNo;
	            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
	        } else {
	            //uiModel.addAttribute("result", GeoHomeZone.findAllGeoHomeZones());
	         //System.out.println("aaa");
	         page =1; size=1;
	         int sizeNo = size == null ? 10 : size.intValue();
	            uiModel.addAttribute("result",ThresholdBalTypeMap.findAllThresholdBalTypeMaps());
	            float nrOfPages = (float) ThresholdBalTypeMap.countThresholdBalTypeMaps() / sizeNo;
	            
	            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
	         
	        }
	  
	  //result = GeoHomeZone.findAllGeoHomeZones();
	  return result;
	 }
	
	
}
