package com.nw.web;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nw.tree.node.TreeNode;
import com.nw.tree.node.TreeNodeUtil;

@Controller
@RequestMapping("/nestedsets/**")
public class NestedSetController {
	
	@RequestMapping(value="/nestedsets/getNode", method=RequestMethod.GET, headers="Accept=application/json")
	@ResponseBody
	public Object getNode(@RequestParam("treeType") int treeType, @RequestParam("id") int id) {
		
		TreeNode treeNode = TreeNodeUtil.getInstance(treeType, id);
		if (treeNode != null) {
			return treeNode.getNode();
		}
		
		return null;
	}
}
